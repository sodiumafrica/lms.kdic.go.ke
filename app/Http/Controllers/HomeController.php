<?php

namespace App\Http\Controllers;

use App\Mail\ContactAdmin;
use App\Models\Blog;
use App\Models\Config;
use App\Models\Course;
use App\Models\CourseProgress;
use App\Models\CourseSection;
use App\Models\Exam;
use App\Models\Lesson;
use App\Models\Question;
use App\Models\Score;
use App\Models\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth', ['except' => ['checkUserEmailExists']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $course = Course::get()->first();

        return redirect('course-view/' . $course->course_slug);

        //        if(auth()->id()) {
        ////            return redirect('my-courses');
        //        }
        //
        //        return view('site/home', [
        //            'courses' => Course::get()
        //        ]);
    }

    /**
     * Function to check whether the email already exists
     *
     * @param array $request All input values from form
     *
     * @return true or false
     */
    public function checkUserEmailExists(Request $request)
    {
        $email = $request->input('email');

        $users = User::where('email', $email)->first();

        echo $users ? "false" : "true";
    }

    public function blogList(Request $request)
    {
        $paginate_count = 3;
        $blogs = Blog::where('is_active', 1)
            ->paginate($paginate_count);

        $archieves = DB::table('blogs')
            ->select(DB::raw('YEAR(created_at) year, MONTH(created_at) month, MONTHNAME(created_at) month_name, COUNT(*) blog_count'))
            ->groupBy('year')
            ->groupBy('month')
            ->orderBy('year', 'desc')
            ->orderBy('month', 'desc')
            ->get();

        return view('site.blogs.list', compact('blogs', 'archieves'));
    }

    public function blogView($blog_slug = '', Request $request)
    {
        $paginate_count = 1;
        $blog = Blog::where('blog_slug', $blog_slug)->first();

        $archieves = DB::table('blogs')
            ->select(DB::raw('YEAR(created_at) year, MONTH(created_at) month, MONTHNAME(created_at) month_name, COUNT(*) blog_count'))
            ->groupBy('year')
            ->groupBy('month')
            ->orderBy('year', 'desc')
            ->orderBy('month', 'desc')
            ->get();

        return view('site.blogs.view', compact('blog', 'archieves'));
    }

    public function pageAbout(Request $request)
    {
        return view('site.pages.about');
    }

    public function pageContact(Request $request)
    {
        return view('site.pages.contact');
    }

    public function contactAdmin(Request $request)
    {
        $admin_email = Config::get_option('settingGeneral', 'admin_email');
        Mail::to($admin_email)->send(new ContactAdmin($request));
        return $this->return_output('flash', 'success', 'Thanks for your message, will contact you shortly', 'back', '200');
    }

    public function getCheckTime()
    {
        $reset_site_at = Config::get_option('lastResetTime', 'lastResetTime');

        $reset_minutes = 60 * 60;

        if (($reset_site_at + $reset_minutes) - time() > 0) {
            echo ($reset_site_at + $reset_minutes) - time();
        } else {
            echo $reset_minutes;
        }
    }

    function class($course_slug, $lecture_slug)
    {
        $course = Course::with('sections.lessons')->where('course_slug', $course_slug)->first();
        $lesson = Lesson::with(['_media', 'progress', 'section.exam.questions'])->find(\SiteHelpers::encrypt_decrypt($lecture_slug, 'd'));
        $sections = CourseSection::with('lessons')->where('course_id', $course->id)->get();

        $progress = CourseProgress::where([
            'user_id'    => auth()->id(),
            'course_id'  => $course->id,
            'lecture_id' => $lesson->lecture_quiz_id,
        ])->first();

        if (!$progress) {
            $progress = CourseProgress::create([
                'user_id'     => auth()->id(),
                'course_id'   => $course->id,
                'lecture_id'  => $lesson->lecture_quiz_id,
                'status'      => 0,
                'score'       => 10,
                'created_at'  => now(),
                'modified_at' => now()

            ]);
            $lesson = Lesson::with(['_media', 'progress'])->find(\SiteHelpers::encrypt_decrypt($lecture_slug, 'd'));
        }

        # Check if final and give certificate
        # Get lesson sections
        $sectionsx = collect([]);
        $examQNCount = 0;
        $examQNIDs = null;
        CourseSection::where('course_id', $course->id)->get()->each(function ($section) use (&$sectionsx, &$examQNCount, &$examQNIDs) {
            $sectionsx->push($section->section_id);
            # Get section exam
            $examQNCount = $examQNCount + Exam::where('section_id', $section->section_id)->first()->questions->count();
            $examQNIDs[] = Exam::where('section_id', $section->section_id)->first()->questions;
        });
        $examQNIDs = collect($examQNIDs)->flatten()->count();
//        dump($sectionsx);

        # Get all lessons in all sections
        $lessonsx = Lesson::whereIn('section_id', $sectionsx)->count();
//        dump($lessonsx);

        # Get all progress records
        $progressx = CourseProgress::where([
            'user_id'   => auth()->id(),
            'course_id' => $course->id,
        ])->count();
//        dump($progressx);

        # Get all exam scores
        $completion['lessons'] = ($lessonsx == $progressx) ? true : false;
        $completion['exams'] = ($examQNCount == $examQNIDs) ? true : false;

        return view('class.index', [
            'course'     => $course,
            'lesson'     => $lesson,
            'sections'   => $sections,
            'completed'  => ($progress->status == 1) ? 'YES' : 'NO',
            'completion' => $completion
        ]);
    }

    function complete($course_slug, $lecture_slug)
    {
        $course = Course::with('sections.lessons')->where('course_slug', $course_slug)->first();
        $lesson = Lesson::find(\SiteHelpers::encrypt_decrypt($lecture_slug, 'd'));

        CourseProgress::where([
            'user_id'    => auth()->id(),
            'course_id'  => $course->id,
            'lecture_id' => $lesson->lecture_quiz_id,
        ])->delete();
        CourseProgress::create([
            'course_id'   => $course->id,
            'user_id'     => auth()->id(),
            'lecture_id'  => $lesson->lecture_quiz_id,
            'status'      => 1,
            'created_at'  => date("Y-m-d H:i:s"),
            'modified_at' => date("Y-m-d H:i:s")
        ]);

        session()->flash('msg', 'You have completed this lesson!');

        return redirect('course-enroll/' . $course_slug . '/' . $lecture_slug);
    }

    function record()
    {
        $question = Question::find(request()->question);
        Score::create([
            'user_id'     => auth()->id(),
            'question_id' => request()->question,
            'answer'      => request()->answer,
            'score'       => ($question->answer == request()->answer) ? $question->score : 0
        ]);

        $question = Question::find(request()->question);

        return response()->json([
            'status'   => 'OK',
            'html'     => view('class.answer-fragment', [
                'qn' => $question
            ])->render(),
            'total'    => $question->exam->total_points,
            'percent'  => round(($question->exam->total_points / $question->exam->max_points) * 100),
            'complete' => $question->exam->complete
        ]);
    }

    function downloadCertificate($encID)
    {
        if (decrypt($encID) != auth()->id()) {
            abort(403, 'Access denied');
        }

        $img = Image::make('certs/KDIC Certificates 2-02.jpg');
        $name = auth()->user()->first_name . ' ' . auth()->user()->last_name;
        $img = $img->text($name, 1800, 1090, function ($font) {
            $font->file('fonts/OpenSans-Bold.ttf');
            $font->size(100);
            $font->color('#000000');
            $font->align('center');
        });

        return $img->response('jpg', 50);
    }
}
