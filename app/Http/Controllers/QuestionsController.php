<?php

namespace App\Http\Controllers;

use App\Models\Exam;
use App\Models\Question;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('instructor.question.list', [
            'exam' => Exam::with('questions')->find(request()->exam)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('instructor.question.create', [
            'exam' => Exam::find(request()->examID)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //        dd(request()->all());

        $validator = Validator::make($request->all(), [
            'question'       => 'required',
            'score'          => 'required',
            'choice'         => 'required',
            'reason'         => 'required',
            'correct_answer' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        Question::create([
            'exam_id'  => request()->exam_id,
            'question' => request()->question,
            'score'    => request()->score,
            'choices'  => request()->choice,
            'reasons'   => request()->reason,
            'answer'   => request()->correct_answer,
        ]);

        return $this->return_output('flash', 'success', 'Question successfully added', 'questions?exam=' . request()->exam_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('instructor.question.edit', [
            'question' => Question::find($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //        return $data=Input::all();
        $validator = Validator::make($request->all(), [
            'question'       => 'required',
            'score'          => 'required',
            'choice'         => 'required',
            'reason'         => 'required',
            'correct_answer' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $question = Question::find($id);

        $question->question = $request->input('question');
        $question->score = $request->input('score');
        $question->choices = $request->input('choice');;
        $question->reasons = $request->input('reason');;
        $question->answer = $request->input('correct_answer');

        $question->save();

        return $this->return_output('flash', 'success', 'Question successfully updated', 'questions?exam=' . request()->exam_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::find($id);

        if (!$question) {
            abort(403, 'Access denied');
        }

        $question->delete();

        return $this->return_output('flash', 'success', 'Question successfully deleted', 'questions?exam=' . $question->exam_id);
    }

    function retake($id)
    {
        $qn = Question::find($id);

        if (!$qn || !$qn->response) {
            return redirect()->back();
        }

        if ($qn->retakes >= 2) {
            return redirect()->back();
        }

        $qn->update([
            'retakes' => ($qn->retakes + 1)
        ]);

        $qn->_response->delete();
        return redirect()->back();
    }
}
