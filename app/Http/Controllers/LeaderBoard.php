<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class LeaderBoard extends Controller
{
    function index()
    {
    	$board = null;

    	User::get()->each(function($user) use (&$board) {
    		$board[] = [
    			'user' => $user,
    			'points' => User::points(1, $user->id)
    		];
    	});

    	# Sort board
    	$board = collect($board)->sortBy('points')->reverse();

    	return view('site.leaderboard', [
    		'board' => $board
    	]);

    	// dd($board);
    }
}
