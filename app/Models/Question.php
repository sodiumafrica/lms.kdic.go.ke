<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Score;

class Question extends Model
{
    protected $guarded = ['id', 'created_at'];
    protected $appends = ['response'];
    protected $casts = [
        'choices' => 'array',
        'reasons' => 'array'
    ];

    function exam()
    {
        return $this->belongsTo(Exam::class, 'exam_id', 'id');
    }

    function getResponseAttribute()
    {
        return Score::where([
            'user_id' => auth()->id(),
            'question_id' => $this->attributes['id']
        ])->first();
    }

    function _response()
    {
        return $this->getResponseAttribute();
    }

}

