<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SiteHelpers;

class Lesson extends Model
{
    protected $table = 'curriculum_lectures_quiz';
    protected $guarded = ['id'];
    protected $primaryKey = 'lecture_quiz_id';

    protected $casts = [
        'resources' => 'array'
    ];

    protected $appends = [
        'completed', 'url', 'lesson_position', 'next', 'prev'
    ];

    function _media()
    {
        return $this->hasOne(CourseFiles::class, 'id', 'media');
    }

    function getCompletedAttribute()
    {
        $section = CourseSection::find($this->attributes['section_id']);

        // dd($section);

        $progress = CourseProgress::where('user_id', auth()->id())
            ->where('course_id', $section->course_id)
            ->where('lecture_id', $this->attributes['lecture_quiz_id'])
            ->where('status', 1)
            ->first();

        return ($progress) ? 'YES' : 'NO';
    }

    function getUrlAttribute()
    {
        return SiteHelpers::encrypt_decrypt($this->attributes['lecture_quiz_id']);
    }

    function getLessonPositionAttribute()
    {
        $lessons = CourseSection::find($this->attributes['section_id'])->lessons;

        $return = NULL;
        $count = 1;
        collect($lessons)->each(function ($lesson) use (&$return, &$count) {
            if ($this->attributes['lecture_quiz_id'] == $lesson->lecture_quiz_id) {
                $return = [
                    'count' => $count
                ];
            }
            $count++;
        });
        $return['total'] = count($lessons);

        return $return;
    }

    function getPrevAttribute()
    {
        return @SiteHelpers::encrypt_decrypt(Lesson::where('lecture_quiz_id', '<', $this->attributes['lecture_quiz_id'])->first()->lecture_quiz_id);
    }

    function getNextAttribute()
    {
        return @SiteHelpers::encrypt_decrypt(Lesson::where('lecture_quiz_id', '>', $this->attributes['lecture_quiz_id'])->first()->lecture_quiz_id);
    }

    function progress()
    {
        return $this->hasOne(CourseProgress::class, 'lecture_id', 'lecture_quiz_id');
    }

    function section()
    {
        return $this->hasOne(CourseSection::class, 'section_id', 'section_id');
    }
}
