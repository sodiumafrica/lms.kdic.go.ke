<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id', 'created_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function instructor()
    {
        return $this->hasOne('App\Models\Instructor', 'user_id', 'id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * @param string|array $roles
     */
    public function authorizeRoles($roles)
    {
        if (is_array($roles)) {
            return $this->hasAnyRole($roles) ||
                abort(401, 'This action is unauthorized.');
        }
        return $this->hasRole($roles) ||
            abort(401, 'This action is unauthorized.');
    }

    /**
     * Check multiple roles
     * @param array $roles
     */
    public function hasAnyRole($roles)
    {
        return NULL !== $this->roles()->whereIn('name', $roles)->first();
    }

    /**
     * Check one role
     * @param string $role
     */
    public function hasRole($role)
    {
        return NULL !== $this->roles()->where('name', $role)->first();
    }

    public function RoleUser()
    {
        return $this->hasMany('App\Models\RoleUser', 'user_id', 'id');
    }

    static function points($courseID, $userID = null)
    {
        $total = 0;
        if(!$userID) {
            $userID = auth()->id();
        }

        # Earn points for every lesson completed
        $progress = CourseProgress::where([
            'user_id'   => $userID,
            'course_id' => $courseID,
            'status'    => 1
        ])->get();

        $progress->each(function($item) use (&$total){
            $total = $total + $item->score;
        });

        # Earn points for every right exam answer
        Score::where([
            'user_id' => auth()->id(),
        ])->each(function($score) use (&$total) {
            $total = $total + $score->score;
        });

        return $total;
    }

}
