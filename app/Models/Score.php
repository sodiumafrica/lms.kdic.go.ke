<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    protected $guarded = ['id', 'created_at'];

    protected $appends = ['correct', 'answer_index'];

    function getCorrectAttribute()
    {
        $qn = Question::find($this->attributes['question_id']);

        return ($qn->answer == $this->attributes['answer']) ? true : false;
    }

    function getAnswerIndexAttribute()
    {
        $qn = Question::find($this->attributes['question_id']);
        $correct = $this->attributes['answer'];

        $index = 0;
        collect($qn->answers)->each(function($answer) use (&$index, $correct) {
            if($correct == $answer) {
                return false; # break
            }
            $index++;
        });

        return $index;
    }
}
