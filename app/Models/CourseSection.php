<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use SiteHelpers;

class CourseSection extends Model
{
    protected $table = 'curriculum_sections';
    protected $guarded = ['id', 'created_at'];
    protected $appends = ['next'];

    protected $primaryKey = 'section_id';

    function lessons()
    {
        return $this->hasMany(Lesson::class, 'section_id', 'section_id');
    }

    function exam()
    {
        return $this->hasOne(Exam::class, 'section_id', 'section_id');
    }

    function getNextAttribute()
    {
        $sec = CourseSection::where('section_id', '>', $this->attributes['section_id'])->first();

        if ($sec) {
            $next = $sec->lessons->first()->lecture_quiz_id;
        }

        return ($sec) ? @SiteHelpers::encrypt_decrypt($next) : null;
    }
}
