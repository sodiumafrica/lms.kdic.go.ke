<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseFiles extends Model
{

    protected $table = 'course_files';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    public $timestamps = false;

    public function getDateFormat()
    {
        return 'U';
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

    function getPathAttribute($val)
    {
        return asset('storage/'.$val);
    }
}