<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Exam;

class Exam extends Model
{
    protected $guarded = ['id', 'created_at'];
    protected $appends = ['total_points', 'max_points', 'complete'];

    function questions()
    {
        return $this->hasMany(Question::class, 'exam_id', 'id');
    }

    function getTotalPointsAttribute()
    {
    	$qns = Exam::find($this->attributes['id'])->questions;

    	$points = 0;
    	collect($qns)->each(function($qn) use (&$points) {
    		if($qn->response) {
    			$points = $points + $qn->response->score;
    		}
    	});

    	return $points;
    }

    function getMaxPointsAttribute()
    {
    	$qns = Exam::find($this->attributes['id'])->questions;

    	$points = 0;
    	collect($qns)->each(function($qn) use (&$points) {
    		$points = $points + $qn->score;
    	});

    	return $points;
    }

    function getCompleteAttribute()
    {
    	$qns = Exam::find($this->attributes['id'])->questions;

    	$state = true;
    	collect($qns)->each(function($qn) use (&$state) {
    		if(!$qn->response) {
    			$state = false;
    		}
    	});

    	return $state;
    }
}
