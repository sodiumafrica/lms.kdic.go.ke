@extends('layouts.frontend.index')
@section('content')
<!-- content start -->
    <div class="container-fluid p-0 home-content">

        <div class="subpage-slide-blue mb-5">
            <div class="container">
                <h1>Leaderboard</h1>
            </div>
        </div>
       


        <!-- course list start -->
        <div class="container tab-content">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <tr>
                            <th>Names</th>
                            <th>Bank</th>
                            <th>Score</th>
                        </tr>
                        @foreach($board as $row)
                            <tr>
                                <td>{{ $row['user']->first_name }} {{ $row['user']->last_name }}</td>
                                <td>
                                    @if($row['user']->bank)
                                        {{ $row['user']->bank }}
                                    @else
                                        Not available
                                    @endif
                                </td>
                                <td>{{ $row['points'] }} points</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                
            </div>
        </div>
        <!-- course list end -->

    </div>
    <!-- content end -->
@endsection