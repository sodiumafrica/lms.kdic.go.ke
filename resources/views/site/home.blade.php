@extends('layouts.frontend.index')
@section('content')
<!-- content start -->
    <div class="container-fluid p-0 home-content">
        <!-- banner start -->
        <div class="homepage-slide-blue pt-5">
            <h1 class="mt-3">{{ Sitehelpers::get_option('pageHome', 'banner_title') }}</h1>
            <span class="title-sub-header">{{ Sitehelpers::get_option('pageHome', 'banner_text') }}</span>
        </div>
        <!-- banner end -->

        <?php
        $tabs = array(
            'latestTab'   => 'Latest Courses',
//            'freeTab'     => 'Free Courses',
//            'discountTab' => 'Discount Courses',
        );
        ?>

        <div class="container">
            <div class="col-md-12 mt-3">
                <h1 class="text-center">Available Courses</h1>
                <hr>
            </div>
        </div>

{{--        {{ dd($courses) }}--}}


        <!-- course list start -->
        <div class="container tab-content">
            <div class="row">
                @foreach($courses as $course)
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                        <div class="course-block mx-auto">
                            <a href="{{ route('course.view', $course->course_slug) }}">
                                <main>
                                    <img class="img-fluid" src="@if(Storage::exists($course->thumb_image)){{ Storage::url($course->thumb_image) }}@else{{ asset('placeholder.png') }}@endif">
                                    <div class="col-md-12"><h6 class="course-title">{{ $course->course_title }}</h6></div>
                                </main>
                                <footer>
                                    <div class="c-row">
                                        {{--<div class="col-md-6 col-sm-6 col-6">--}}
                                            {{--@php $course_price = $course->price ? config('config.default_currency').$course->price : 'Free'; @endphp--}}
                                            {{--<h5 class="course-price">{{  $course_price }}&nbsp;<s>{{ $course->strike_out_price ? $course->strike_out_price : '' }}</s></h5>--}}
                                        {{--</div>--}}
                                        <div class="col-md-5 offset-md-1 col-sm-5 offset-sm-1 col-5 offset-1">
                                            <star class="course-rating">
                                                @for ($r=1;$r<=5;$r++)
                                                    <span class="fa fa-star {{ $r <= $course->average_rating ? 'checked' : '' }}"></span>
                                                @endfor
                                            </star>
                                        </div>
                                    </div>
                                </footer>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <!-- course list end -->

    </div>
    <!-- content end -->
@endsection