@extends('layouts.frontend.index')
@section('content')
<!-- content start -->
    <div class="container-fluid p-0 home-content">
        <!-- banner start -->
        <div class="subpage-slide-blue">
            <div class="container">
                <h1>Enroll Course</h1>
            </div>
        </div>
        <!-- banner end -->

         <!-- breadcrumb start -->
            <div class="breadcrumb-container">
                <div class="container">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Enroll course</li>
                  </ol>
                </div>
            </div>
        
        <!-- breadcrumb end -->
        
        
        <article class="container not-found-block">
            <div class="row">
               <div class="col-12 not-found-col">
                    <span><i class="fas fa-thumbs-up"></i></span>
                    <h6 class="my-3">You have successfully enrolled for this course</h6>
                    <a href="{{ route('course.learn', $course->course_slug) }}" class="btn btn-success mt-3 btn-lg">
                        <i class="fa fa-smile fa-fw"></i> Start learning this course
                    </a>
               </div>
            </div>
        </article>
        
        
    <!-- content end -->
@endsection
