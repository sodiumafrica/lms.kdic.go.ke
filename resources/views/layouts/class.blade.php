<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Panel Structure">
    <title>Banker's Classroom</title>

    <script src="https://kit.fontawesome.com/fa5962e5d7.js" crossorigin="anonymous"></script>

</head>
<body class="animsition site-menubar-unfold" cz-shortcut-listen="true">
<div id="course-enroll-container">

    <div>
        <div id="top-bar">

            <nav className="site-navbar navbar navbar-default navbar-fixed-top navbar-mega navbar-expand-lg" role="navigation">
                <div className="navbar-header">
                    <button type="button" className="navbar-toggler hamburger hamburger-close navbar-toggler-left hided" data-toggle="menubar">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="hamburger-bar"></span>
                    </button>
                    <div className="navbar-brand navbar-brand-center site-gridmenu-toggle active" data-toggle="gridmenu" aria-expanded="true">
                        <span className="navbar-brand-text hidden-xs-down"> KDIC Banker's Classroom</span>
                    </div>
                </div>

                <div className="navbar-container container-fluid">
                    <div className="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
                        <ul className="nav navbar-toolbar">
                            <li className="nav-item hidden-float" id="toggleMenubar">
                                <a className="nav-link" data-toggle="menubar" href={site_url + "/course-learn/" + course_slug} role="button">
                                <FontAwesomeIcon icon="arrow-left"/> &nbsp;Back to course summary
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

        </div>
        <div className="site-menubar" id="site-menubar">
            <div className="site-menubar-body">
                <ul className="site-menu">
                    {this.sideItem()}
                </ul>
            </div>
        </div>
        <div className="page">
            <CourseContent lecture={this.state.lecture_detail} courseID={this.state.course_id}/>
        </div>
        <div className="site-action">
            {/*<Resource lecture={this.state.lecture_detail}/>*/}
        </div>
    </div>

</div>
</body>

<script src="{{ asset('frontend/js/jquery-3.3.1.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.navbar-toggler').click(function () {
            $(this).toggleClass('hided');
            $('.page').toggleClass('sidebar-open');
            $('.site-menubar').toggleClass('sidebar-open');
        });

    });

    var site_url = '{{ url("/") }}';
    var base_url = window.location.pathname;
    base_url = base_url.slice(0, base_url.lastIndexOf('/'));
    var storage_url = '{{ Storage::url('/course/') }}';
    var course_slug = '{{ Request::segment(2) }}';
    var lecture_slug = '{{ Request::segment(3) }}';
</script>
<script src="{{ asset('js/app.js') }}"></script>
</html>