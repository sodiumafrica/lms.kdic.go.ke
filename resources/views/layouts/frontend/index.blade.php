<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name') }}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/fancybox.css') }}">

    <link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.css') }}">

    <link rel="stylesheet" href="{{ asset('backend/fonts/web-icons/web-icons.min599c.css?v4.0.2') }}">
    <link rel="stylesheet" href="{{ asset('backend/vendor/toastr/toastr.min599c.css?v4.0.2') }}">

    <script src="https://kit.fontawesome.com/fa5962e5d7.js" crossorigin="anonymous"></script>

</head>
<body>
<div class="se-pre-con"></div>
<!-- Header -->

<nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top" style="box-shadow: 1px 1px 8px -3px rgba(0,0,0,0.75);">
    <div class="container">
        <a class="navbar-brand" href="/">
            <img src="{{ asset('frontend/img/logo.png') }}" width="150"/></a>
        </a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse collapse" id="navbarColor01" style="">

            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('home') }}">
                        <i class="fas fa-home fa-fw"></i> Home
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/leaderboard">
                        <i class="fas fa-award fa-fw"></i> Leader Board
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" target="_blank" href="https://kdic.go.ke">
                        <i class="fas fa-paperclip fa-fw"></i> KDIC Website
                    </a>
                </li>
            </ul>


            <ul class="navbar-nav ml-auto">
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">
                            <i class="fas fa-lock fa-fw"></i> Register
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">
                            <i class="fas fa-plus-circle fa-fw"></i> Login
                        </a>
                    </li>
                @else
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-user-circle-o fa-fw"></i> Hello, {{ Auth::user()->first_name }}!
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @if(Auth::user()->hasRole('instructor'))
                                <a class="dropdown-item" href="{{ route('instructor.dashboard') }}">
                                    <i class="fa fa-cogs fa-fw"></i> Instructor
                                </a>
                            @endif
                            <a class="dropdown-item" href="{{ route('my.courses') }}">
                                <i class="fa fa-list fa-fw"></i> My Courses
                            </a>

                            <a class="dropdown-item" href="{{ route('logOut') }}">
                                <i class="fa fa-sign-out-alt"></i> Logout
                            </a>
                        </div>
                    </div>
                @endguest
            </ul>

        </div>
    </div>
</nav>


@yield('content')

<!-- footer start -->
<footer id="main-footer">
    <div class="row m-0">

        <div class="col-md-12 mt-4">
            <ul class="nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link active" href="/">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="@if(auth()->id()) /my-courses @else /login @endif">
                        @if(auth()->id()) My Courses @else Login Page @endif
                    </a>
                </li>
                <li class="nav-item">
                    <a target="_blank" class="nav-link" href="https://kdic.go.ke">KDIC Website</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="/contact">Contact Us</a>
                </li>
            </ul>
        </div>

        <div class="col-md-12 text-center mt-4 text-white">
            Copyright © {{ date('Y') }}, {{ config('app.name') }}. All Rights Reserved.
        </div>

    </div>
</footer>
<!-- footer end -->

<!-- The Modal start -->
<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bi-header ">
                <h5 class="col-12 modal-title text-center bi-header-seperator-head">Become an Instructor</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="becomeInstructorForm">
                <form id="becomeInstructorForm" class="form-horizontal" method="POST" action="{{ route('become.instructor') }}">
                    {{ csrf_field() }}
                    <div class="px-4 py-2">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-6">
                                    <label>First Name</label>
                                    <input type="text" class="form-control form-control-sm" placeholder="First Name" name="first_name">
                                </div>
                                <div class="col-6">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control form-control-sm" placeholder="Last Name" name="last_name">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Contact Email</label>
                            <input type="text" class="form-control form-control-sm" placeholder="Contact Email" name="contact_email">
                        </div>

                        <div class="form-group">
                            <label>Telephone</label>
                            <input type="text" class="form-control form-control-sm" placeholder="Telephone" name="telephone">
                        </div>

                        <div class="form-group">
                            <label>Paypal ID</label>
                            <input type="text" class="form-control form-control-sm" placeholder="Paypal ID" name="paypal_id">
                        </div>

                        <div class="form-group">
                            <label>Biography</label>
                            <textarea class="form-control form-control" placeholder="Biography" name="biography"></textarea>
                        </div>

                        <div class="form-group mt-4">
                            <button type="submit" class="btn btn-lg btn-block login-page-button">Submit</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- The Modal end -->
</body>
<script src="{{ asset('frontend/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('frontend/js/fancybox.min.js') }}"></script>
<script src="{{ asset('frontend/js/modernizr.js') }}"></script>
<script src="{{ asset('frontend/js/jquery.validate.js') }}"></script>

<!-- Toastr -->
<script src="{{ asset('backend/vendor/toastr/toastr.min599c.js?v4.0.2') }}"></script>


<script>
    $(window).on("load", function (e) {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        /* Delete record */
        $('.delete-record').click(function (event) {
            var url = $(this).attr('href');
            event.preventDefault();

            if (confirm('Are you sure want to delete this record?')) {
                window.location.href = url;
            } else {
                return false;
            }

        });

        /* Toastr messages */
        toastr.options.closeButton = true;
        toastr.options.timeOut = 5000;
        @if(session()->has('success'))
        toastr.success("{{ session('success') }}");
        @endif
        @if(session()->has('status'))
        toastr.success("{{ session('status') }}");
        @endif
        @if(session()->has('error'))
        toastr.error("{{ session('error') }}");
        @endif
        @if(session()->has('info'))
        toastr.info("{{ session('info') }}");
        @endif

        $('.mobile-nav').click(function () {
            $('#sidebar').toggleClass('active');

            $(this).toggleClass('fa-bars');
            $(this).toggleClass('fa-times');
        });

        $("#becomeInstructorForm").validate({
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                contact_email: {
                    required: true,
                    email: true
                },
                telephone: {
                    required: true
                },
                paypal_id: {
                    required: true,
                    email: true
                },
                biography: {
                    required: true
                },
            },
            messages: {
                first_name: {
                    required: 'The first name field is required.'
                },
                last_name: {
                    required: 'The last name field is required.'
                },
                contact_email: {
                    required: 'The contact email field is required.',
                    email: 'The contact email must be a valid email address.'
                },
                telephone: {
                    required: 'The telephone field is required.'
                },
                paypal_id: {
                    required: 'The paypal id field is required.',
                    email: 'The paypal id must be a valid email address.'
                },
                biography: {
                    required: 'The biography field is required.'
                },
            }
        });
    });
</script>
@yield('javascript')
</html>