<div class="col-md-12 mt-4">
    <div class="progress">
        <div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width: {{ $progress['percent'] }}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">{{ $progress['percent'] }}% complete</div>
    </div>
</div>
<div class="text-center">
    You have completed {{ $progress['count'] }} of {{ $progress['total'] }} lessons in this course
</div>

