@extends('layouts.frontend.index')

@section('content')
<!-- content start -->
    <div class="container-fluid p-0 home-content container-top-border">
        <!-- account block start -->
        <div class="container">
            <nav class="navbar clearfix secondary-nav pt-0 pb-0 login-page-seperator">
                <ul class="list mt-0">
                     <li><a href="{{ route('login') }}" >
                             <i class="fas fa-lock fa-fw"></i> Login
                         </a>
                     </li>
                     <li>
                         <a href="{{ route('register') }}" class="active">
                             <i class="fas fa-plus-circle fa-fw"></i> Register
                         </a>
                     </li>
                </ul>
            </nav>

            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 vertical-align d-none d-lg-block mt-5">
                    <img class="img-fluid" src="http://placehold.it/800x600?text=800x600">
                </div>
                <div class="col-xl-6 offset-xl-0 col-lg-6 offset-lg-0 col-md-8 offset-md-2">
                    <div class="rightRegisterForm">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}" id="registerForm">
                        {{ csrf_field() }}
                        <div class="p-4">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-6">
                                        <label>First Name</label>
                                        <input type="text" class="form-control form-control-sm" placeholder="First Name" value="@if(!empty($name)){{ $name }}@else{{ old('first_name') }}@endif" name="first_name"   >
                                        @if ($errors->has('first_name'))
                                        <label class="error" for="first_name">{{ $errors->first('first_name') }}</label>
                                        @endif
                                    </div>
                                    <div class="col-6">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control form-control-sm" placeholder="Last Name" value="{{ old('last_name') }}" name="last_name">
                                        @if ($errors->has('last_name'))
                                        <label class="error" for="last_name">{{ $errors->first('last_name') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-6">
                                        <label>Email Address</label>
                                        <input type="text" class="form-control form-control-sm" placeholder="Email ID" value="@if(!empty($name)){{ $email }}@else{{ old('email') }}@endif" name="email">
                                        @if ($errors->has('email'))
                                            <label class="error" for="email">{{ $errors->first('email') }}</label>
                                        @endif
                                    </div>
                                    <div class="col-6">
                                        <label>Telephone</label>
                                        <input type="text" class="form-control form-control-sm" placeholder="Telephone" value="{{ old('telephone') }}" name="telephone">
                                        @if ($errors->has('telephone'))
                                        <label class="error" for="telephone">{{ $errors->first('telephone') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-6">
                                        <label>Date of Birth</label>
                                        <input type="date" class="form-control form-control-sm" placeholder="Date of Birth" name="dob">
                                        @if ($errors->has('dob'))
                                            <label class="error" for="dob">{{ $errors->first('dob') }}</label>
                                        @endif
                                    </div>
                                    <div class="col-6">
                                        <label>Gender</label>
                                        <select name="gender" class="form-control">
                                            <option selected value="">Choose a gender</option>
                                            <option value="M">Male</option>
                                            <option value="F">Female</option>
                                        </select>
                                        @if ($errors->has('gender'))
                                            <label class="error" for="gender">{{ $errors->first('gender') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-6">
                                        <label>Section</label>
                                        <select name="section" id="" class="form-control">
                                            <option selected value="">Choose a section</option>
                                            @foreach(config('settings.sections') as $key => $value)
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('section'))
                                            <label class="error" for="section">{{ $errors->first('section') }}</label>
                                        @endif
                                    </div>
                                    <div class="col-6">
                                        <label>Job Grade</label>
                                        <select name="job_grade" id="" class="form-control">
                                            <option selected value="">Choose a job grade</option>
                                            @foreach(config('settings.job_grade') as $key => $value)
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('job_grade'))
                                            <label class="error" for="job_grade">{{ $errors->first('job_grade') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-6">
                                        <label>County</label>
                                        <select name="county" id="" class="form-control">
                                            <option selected value="">Choose a county</option>
                                            @foreach(config('settings.counties') as $key => $value)
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('county'))
                                            <label class="error" for="county">{{ $errors->first('county') }}</label>
                                        @endif
                                    </div>
                                    <div class="col-6">
                                        <label>Bank</label>
                                        <select name="bank" id="" class="form-control">
                                            <option selected value="">Choose a bank</option>
                                            @foreach(config('settings.banks') as $key => $value)
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('bank'))
                                            <label class="error" for="bank">{{ $errors->first('bank') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control form-control-sm" placeholder="Password" name="password" id="password">
                                @if ($errors->has('password'))
                                <label class="error" for="password">{{ $errors->first('password') }}</label>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input type="password" class="form-control form-control-sm" placeholder="Confirm Password" name="password_confirmation">
                                @if ($errors->has('password_confirmation'))
                                <label class="error" for="password_confirmation">{{ $errors->first('password_confirmation') }}</label>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="offer" name="offer" {{ old('offer') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="offer">Receive relevant offers & communications</label>
                                </div>
                            </div>

                            <div class="form-group mt-4">
                                <button type="submit" class="btn btn-lg btn-block login-page-button">Register</button>
                            </div>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- account block end -->
    </div>
    <!-- content end -->
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function()
{
    $("#registerForm").validate({
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                email:{
                    required: true,
                    email:true,
                    remote: "{{ url('checkUserEmailExists') }}"
                },
                password:{
                    required: true,
                    minlength: 6
                },
                password_confirmation:{
                    required: true,
                    equalTo: '#password'
                }
            },
            messages: {
                first_name: {
                    required: 'The fname field is required.'
                },
                last_name: {
                    required: 'The lname field is required.'
                },
                email: {
                    required: 'The email field is required.',
                    email: 'The email must be a valid email address.',
                    remote: 'The email has already been taken.'
                },
                password: {
                    required: 'The password field is required.',
                    minlength: 'The password must be at least 6 characters.'
                },
                password_confirmation: {
                    required: 'The password confirmation field is required.',
                    equalTo: 'The password confirmation does not match.'
                }
            }
        });

});
</script>
@endsection