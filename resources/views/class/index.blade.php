<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title>KDIC Classroom</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.css') }}">

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top"
     style="box-shadow: 1px 1px 8px -3px rgba(0,0,0,0.75);">
    <div class="container">
        <a class="navbar-brand" href="/">
            <img src="{{ asset('frontend/img/logo.png') }}" width="150"/></a>
        </a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarColor01"
                aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse collapse" id="navbarColor01" style="">

            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('home') }}">
                        <i class="fas fa-home fa-fw"></i> Home
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/leaderboard">
                        <i class="fas fa-award fa-fw"></i> Leader Board
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="#">
                        <i class="fas fa-paperclip fa-fw"></i> KDIC Website
                    </a>
                </li>
            </ul>


            <ul class="navbar-nav ml-auto">
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">
                            <i class="fas fa-lock fa-fw"></i> Register
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">
                            <i class="fas fa-plus-circle fa-fw"></i> Login
                        </a>
                    </li>
                @else
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-user-circle-o fa-fw"></i> Hello, {{ Auth::user()->first_name }}!
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @if(Auth::user()->hasRole('instructor'))
                                <a class="dropdown-item" href="{{ route('instructor.dashboard') }}">
                                    <i class="fa fa-cogs fa-fw"></i> Instructor
                                </a>
                            @endif
                            <a class="dropdown-item" href="{{ route('my.courses') }}">
                                <i class="fa fa-list fa-fw"></i> My Courses
                            </a>

                            <a class="dropdown-item" href="{{ route('logOut') }}">
                                <i class="fa fa-sign-out-alt"></i> Logout
                            </a>
                        </div>
                    </div>
                @endguest
            </ul>

        </div>
    </div>
</nav>


<div class="container" style="margin-top: 80px;">
    <header class="blog-header py-3">
        <div class="row flex-nowrap justify-content-between align-items-center">
            <div class="col-4 pt-1">
                <a class="text-muted" href="/course-learn/{{ $course->course_slug }}">Go Back</a>
            </div>
            <div class="col-4 text-center">
                <a class="blog-header-logo text-dark" href="#">KDIC Classroom</a>
            </div>
            <div class="col-4 d-flex justify-content-end align-items-center">
                <h5>Total Points <span class="badge badge-primary">{{ \App\Models\User::points($course->id) }}</span>
                </h5>
            </div>
        </div>
    </header>

    @if(!request()->has('exam'))
        <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
            <div class="col-md-8 px-0">
                <h1 class="display-4 font-italic">{{ $lesson->title }}</h1>
                <p class="lead my-3">
                    {{ $lesson->description }}
                </p>
            </div>
        </div>
    @endif
</div>

<main role="main" class="container">
    <div class="row">
        <div class="col-md-8 blog-main">

            <div class="blog-post">
                @if(!request()->has('exam'))
                    @if($lesson->completed == 'YES')
                        @if($lesson->lesson_position['count'] != $lesson->lesson_position['total'])
                            <a class="btn btn-outline-success float-right"
                               href="/course-enroll/{{ $course->course_slug }}/{{ $lesson->next }}">
                                Next Lesson
                            </a>
                        @else
                            @if($lesson->section->exam)
                                <a class="btn btn-success float-right"
                                   href="/course-enroll/{{ $course->course_slug }}/{{ $lesson->url }}?exam">
                                    Take Quiz
                                </a>
                            @else
                                <a class="btn btn-success"
                                   href="/course-enroll/{{ $course->course_slug }}/{{ $lesson->next }}">
                                    Move to next section
                                </a>
                            @endif
                        @endif
                    @endif

                    <h2 class="blog-post-title">Lesson Contents</h2>
                    <hr>

                    @if(session()->flash('msg'))
                        <div class="alert alert-success">
                            {{ session()->flash('msg') }}
                        </div>
                    @endif

                    @if($lesson->completed == 'NO')
                        <div class="alert alert-primary">
                            You have not completed this lesson
                        </div>
                    @else
                        <div class="alert alert-success">
                            You have successfully completed this lesson
                        </div>
                    @endif

                    <div class="text-justify mb-4">
                        @if($lesson->media_type == 'TEXT')
                            {!! $lesson->contenttext !!}
                        @endif

                        @if($lesson->media_type == 'VIDEO')
                            <div className="col d-flex align-items-center justify-content-center">
                                <video controls width="100%">
                                    <source src="{{ $lesson->_media->path }}" type="video/mp4"/>
                                </video>
                            </div>
                        @endif

                        @if($lesson->media_type == 'AUDIO')
                            <div className="col d-flex align-items-center justify-content-center">
                                <audio controls>
                                    <source src="{{ $lesson->_media->path }}" type="audio/mpeg"/>
                                </audio>
                            </div>
                        @endif

                        @if($lesson->media_type == 'PDF')
                            <div className="col mt-4">
                                <iframe src="{{ $lesson->_media->path }}" width="100%" height="600px"></iframe>
                            </div>
                        @endif

                        @if($lesson->media_type == 'PICTURE')
                            <div className="col d-flex align-items-center justify-content-center">
                                <img src="{{ $lesson->_media->path }}" class="img-fluid"/>
                            </div>
                        @endif
                    </div>

                    <hr>
                    <div class="text-muted mb-3">
                        Reward: {{ $lesson->progress->score }} points
                    </div>

                    <nav class="blog-pagination mb-5">
                        @if($lesson->completed == 'NO')
                            <form method="POST" action="/course-enroll/{{ $course->course_slug }}/{{ $lesson->url }}">
                                @csrf()
                                <button class="btn btn-success btn-lg">Complete Lesson</button>
                            </form>
                        @else
                            @if($lesson->lesson_position['count'] != 1)
                                <a class="btn btn-outline-primary"
                                   href="/course-enroll/{{ $course->course_slug }}/{{ $lesson->prev }}">Previous Lesson
                                </a>
                            @endif
                            @if($lesson->lesson_position['count'] != $lesson->lesson_position['total'])
                                <a class="btn btn-outline-secondary"
                                   href="/course-enroll/{{ $course->course_slug }}/{{ $lesson->next }}">
                                    Next Lesson
                                </a>
                            @else
                                @if($lesson->section->exam)
                                    <a class="btn btn-success"
                                       href="/course-enroll/{{ $course->course_slug }}/{{ $lesson->url }}?exam">
                                        Take Quiz
                                    </a>
                                @else
                                    <a class="btn btn-success"
                                       href="/course-enroll/{{ $course->course_slug }}/{{ $lesson->next }}">
                                        Move to next section
                                    </a>
                                @endif
                            @endif
                        @endif

                    </nav>
                @else

                    @if($lesson->section->next)
                        <a href="/course-enroll/{{ $course->course_slug }}/{{ $lesson->section->next }}"
                           class="btn btn-success btnNextSection @if(!$lesson->section->exam->complete) frm-hidden @endif float-right">
                            Proceed to Next Section
                        </a>
                    @endif

                    <h2 class="blog-post-title">Quick Quiz</h2>
                    <hr>

                    @if($lesson->section->exam)
                        <div class="alert alert-primary">
                            You have scored
                            <span class="frmTotalPoints">{{ @$lesson->section->exam->total_points }}</span>
                            of
                            {{ @$lesson->section->exam->max_points }} points
                            (<span class="frmPercentPoints">{{ round((@$lesson->section->exam->total_points / @$lesson->section->exam->max_points) * 100) }}</span>%)
                            in this quiz
                        </div>

                        @foreach($lesson->section->exam->questions as $qn)
                            <div class="card mb-5">
                                <div class="card-body">
                                    <h6 class="badge badge-dark">{{ $qn->score }} points</h6>
                                    <br>
                                    <h5 class="card-title">{{ $loop->iteration }}. {{ $qn->question }}</h5>

                                    @foreach($qn->choices as $key => $choice)
                                        <div class="form-check">
                                            <input class="form-check-input radio-{{ $qn->id }}" type="radio"
                                                   @if(!$qn->response)
                                                   name="radio-{{ $qn->id }}"
                                                   value="{{ $choice }}" data-qnID="{{ $qn->id }}"
                                                   data-correct="{{ $qn->answer }}"
                                                   data-key="{{ $key }}"
                                                   @endif
                                                   @if($qn->response) disabled @endif
                                                   @if(@$qn->response->answer == $choice) checked @endif
                                            >
                                            <label class="form-check-label" for="radio-{{ $qn->id }}">
                                                {{ $choice }}
                                            </label>
                                        </div>
                                    @endforeach

                                    <div class="col-12 answer{{ $qn->id }}">
                                        @if($qn->response)
                                            @include('class.answer-fragment')
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <script>
                                $(function () {
                                    $('.frm-hidden').hide()
                                    $('.radio-{{ $qn->id }}').click(function () {
                                        $('.answer{{ $qn->id }}').html('Checking your answer...')
                                        $('.radio-' + $(this).data('qnid')).attr('disabled', 'disabled')

                                        $.post('/quiz/record-marks', {
                                            question: $(this).data('qnid'),
                                            answer: $(this).val()
                                        }, function (res) {
                                            $('.answer{{ $qn->id }}').html(res.html)
                                            $('.frmTotalPoints').html(res.total)
                                            $('.frmPercentPoints').html(res.percent)

                                            if (res.complete) {
                                                $('.btnNextSection').show()
                                                $('.radio-' + $(this).data('qnid')).removeAttr('disabled')
                                            }
                                        })
                                    })
                                })
                            </script>
                        @endforeach

                        <div class="jumbotron text-center">
                            <h1 class="display-4">
                                Your score is <span
                                        class="frmPercentPoints">{{ round((@$lesson->section->exam->total_points / @$lesson->section->exam->max_points) * 100) }}</span>%
                            </h1>
                            <hr class="my-4">
                            <p class="lead btnNextSection @if(!$lesson->section->exam->complete) frm-hidden @endif">
                                Congratulations! You have successfully completed this module.
                                @if(!$completion['lessons'] && !$completion['exams'])
                                    Please click the Green button to start the next module
                                @endif
                            </p>
                            <p class="lead">
                                @if($lesson->section->next)
                                    <a href="/course-enroll/{{ $course->course_slug }}/{{ $lesson->section->next }}"
                                       class="btn btn-success btn-lg btnNextSection @if(!$lesson->section->exam->complete) frm-hidden @endif">
                                        Proceed to Next Section
                                    </a>
                                @endif
                            </p>
                        </div>
                    @endif
                @endif
            </div>

            <div class="text-center">
                @if($completion['lessons'] && $completion['exams'])
                    <a href="{{ url('certificate/download/'.encrypt(auth()->user()->id)) }}"
                       class="btn btn-success btn-lg">
                        Download your Certificate of Participation
                    </a>
                @endif
            </div>

            <pre>
{{--                {{ dump($lesson) }}--}}
                {{-- {{ json_encode($lesson->section->exam, JSON_PRETTY_PRINT) }} --}}
            </pre>


        </div><!-- /.blog-main -->

        <aside class="col-md-4 blog-sidebar">
            <div>
                <h2>Course Guide</h2>
                <hr>

                @foreach($sections as $section)
                    <div class="card mb-3">
                        <div class="card-header">
                            <b>{{ $section->title }}</b>
                        </div>
                        <ul class="list-group list-group-flush">
                            @foreach($section->lessons as $less)
                                <li class="list-group-item">
                                    @if($less->completed == 'YES') ✔ @else ⚪ @endif
                                    <a href="/course-enroll/{{ $course->course_slug }}/{{ $less->url }}">{{ $less->title }}</a>
                                    @if(($lesson->lecture_quiz_id == $less->lecture_quiz_id) && !request()->has('exam'))
                                        <span class="badge badge-success pull-right">CURRENT</span>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach


            </div>

        </aside><!-- /.blog-sidebar -->

    </div><!-- /.row -->

</main><!-- /.container -->

<footer id="main-footer">
    <div class="row m-0">

        <div class="col-md-12 mt-4">
            <ul class="nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link active" href="/">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href=" /my-courses ">
                        My Courses </a>
                </li>
                <li class="nav-item">
                    <a target="_blank" class="nav-link" href="https://kdic.go.ke">KDIC Website</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="/contact">Contact Us</a>
                </li>
            </ul>
        </div>

        <div class="col-md-12 text-center mt-4 text-white">
            Copyright © 2020, KDIC LMS. All Rights Reserved.
        </div>

    </div>
</footer>

<script src="{{ asset('frontend/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>

</body>
</html>

<script>
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    })
</script>
