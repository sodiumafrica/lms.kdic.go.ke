<div class="alert alert-{{ ($qn->response->correct) ? 'success' : 'danger' }} mt-3" role="alert">
    <h4>{{ (@$qn->response->correct) ? '😊' : '☹' }} This is {{ (@$qn->response->correct) ? 'correct' : 'incorrect' }}!</h4>
    You Answered: <b>{{ $qn->response->answer }}</b>
    <br>
    <span class="reason-{{ $qn->id }}">Explanation: {{ $qn->reasons[$qn->response->answer_index] }}</span>
</div>

@if($qn->retakes >= 2)
    <div class="text-danger">
        You cannot retake this question anymore because you have answered it {{ $qn->retakes + 1 }} times already
    </div>
@else
    <div>
        <small class="text-muted">
            You can retake this question 3 times
        </small>
    </div>
    <a href="/retake/{{ $qn->id }}">I want to retake this question</a>
@endif