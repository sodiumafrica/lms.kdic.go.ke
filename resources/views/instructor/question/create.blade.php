@extends('layouts.backend.index')
@section('content')
    <div class="page-header">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('instructor.dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ url('exams') }}">Exam</a></li>
            <li class="breadcrumb-item active">Add</li>
        </ol>
        <h1 class="page-title">Add Question</h1>

    </div>

    <div class="page-content">

        <div class="card">
            <div class="card-body">


                <form method="POST" action="{{ url('questions') }}">
                    {{ csrf_field() }}

                    <input type="hidden" value="{{ request()->exam }}" name="exam_id">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label">Question</label>
                                <input type="text" class="form-control" name="question"/>
                                @if ($errors->has('question'))
                                    <label class="error" for="question">{{ $errors->first('question') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label">Score</label>
                                <input type="number" class="form-control" name="score" placeholder="Score"/>
                                @if ($errors->has('score'))
                                    <label class="error" for="score">{{ $errors->first('score') }}</label>
                                @endif
                            </div>
                        </div>
                    </div>

                    <h4>Multiple Choices</h4>
                    <hr>
                    <div class="choices">
                        <div class="row col choice-1">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="choice[]" placeholder="Choice..."/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="reason[]" placeholder="Reason..."/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a href="#!" class="btnAdd">Add another choice</a>

                    <br>
                    <br>

                    <h4 class="pt-5">Correct Answer</h4>
                    <hr>
                    <div class="form-group">
                        <input type="text" class="form-control" name="correct_answer"/>
                    </div>

                    <button type="submit" class="btn btn-primary">Save</button>

                </form>
            </div>
        </div>


        <!-- End Panel Basic -->
    </div>

@endsection

@section('javascript')
    <script type="text/javascript">

        var choiceCount = 2;

        $(document).ready(function () {
            $(".btnAdd").click(function () {
                var element = $('.choice-1').clone().removeClass('choice-1').addClass('choice-' + choiceCount)
                $('.choices').append(element)
                choiceCount++
                element = null
            });
        });
    </script>


@endsection