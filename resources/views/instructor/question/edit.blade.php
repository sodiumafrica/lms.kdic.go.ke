@extends('layouts.backend.index')
@section('content')
    <div class="page-header">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('instructor.dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ url('exams') }}">Exam</a></li>
            <li class="breadcrumb-item active">Edit</li>
        </ol>
        <h1 class="page-title">Edit Question</h1>

    </div>

    <div class="page-content">

        <div class="card">
            <div class="card-body">

                {{--{{ dd($question) }}--}}


                <form method="POST" action="{{ url('questions/'.$question->id) }}">
                    {{ csrf_field() }}
                    @method('PUT')

                    <input type="hidden" value="{{ request()->exam }}" name="exam_id">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label">Question</label>
                                <input type="text" class="form-control" name="question" value="{{ $question->question }}"/>
                                @if ($errors->has('question'))
                                    <label class="error" for="question">{{ $errors->first('question') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label">Score</label>
                                <input type="number" class="form-control" name="score" placeholder="Score" value="{{ $question->score }}"/>
                                @if ($errors->has('score'))
                                    <label class="error" for="score">{{ $errors->first('score') }}</label>
                                @endif
                            </div>
                        </div>
                    </div>


                    <div class="choices">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>Choice</h4>
                            </div>
                            <div class="col-md-6">
                                <h4>Reason</h4>
                            </div>
                        </div>
                        @foreach($question->choices as $key => $choice)
                            <div class="row col choice-{{ $loop->iteration }}">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="choice[]" placeholder="Choice..." value="{{ $choice }}"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="reason[]" placeholder="Reason..." value="{{ $question->reasons[$key] }}"/>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>

                    <br>
                    <br>

                    <h5>Answer</h5>
                    <hr>
                    <div class="form-group">
                        <input type="text" class="form-control" name="correct_answer" value="{{$question->answer}}"/>
                    </div>

                    <button type="submit" class="btn btn-success btn-lg">Save Changes</button>

                </form>
            </div>
        </div>


        <!-- End Panel Basic -->
    </div>


@endsection
