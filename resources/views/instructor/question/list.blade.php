@extends('layouts.backend.index')
@section('content')
    <div class="page-header">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('instructor.dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ url('exams') }}">Exam</a></li>
            <li class="breadcrumb-item active">Add</li>
        </ol>
        <h1 class="page-title">Questions in this exam</h1>
    </div>

    <div class="page-content">

        <div class="card">
            <div class="card-body">
                {{ csrf_field() }}
                <h1>{{ $exam->name}}</h1>
                <hr>
                <a href="{{ url('exams') }}" class=" btn btn-warning btn-sm" type="submit"> Back</a>
                <a href="{{ url('questions/create?exam='.$exam->id) }}" class="btn btn-success btn-sm">Add Questions</a>
                <br>
                <br>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Questions</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($exam->questions as $question)
                        <tr>
                            <td>
                                {{ $question->question}}
                                <ul>
                                    @foreach($question->choices as $key => $choice)
                                        <li>{{ $choice }}</li>
                                    @endforeach
                                </ul>

                            </td>
                            <td>
                                <a href="{{ url('questions/'.$question->id.'/edit?exam='.$exam->id) }}"
                                   class="btn btn-primary btn-sm"> View
                                </a>
                                <a href="{{ url('questions/'.$question->id.'/trash?exam_id='.$exam->id) }}"
                                   onclick="return confirm('Are you sure? This action cannot be reversed')"
                                   class="btn btn-danger btn-sm"> Trash
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>


    <!-- End Panel Basic -->

@endsection

