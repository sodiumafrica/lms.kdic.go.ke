@extends('layouts.backend.index')
@section('content')
    <div class="page-header">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('instructor.dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Exam</li>
        </ol>
        <h1 class="page-title">List of Exams</h1>
    </div>

    <div class="page-content">

        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="btn btn-group float-right">

                        <a href="{{ url('exams/create') }}" class="btn btn-success btn-sm"><i
                                    class="icon wb-plus" aria-hidden="true"></i> Add Exam</a>
                    </div>


                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>EXAM</th>
                            <th colspan="2">QUESTIONS</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($exams as $exam)
                            <tr>
                                <td>{{ $exam->name}}</td>
                                <td>{{ $exam->questions->count()}} questions</td>
                                <td>
                                    <div class="btn-group float-right">
                                        <a href="{{ url('exams/'.$exam->id.'/edit') }}"
                                           class="btn btn-primary btn-sm">Edit</a>                                  </a>
                                        <a href="{{ url('questions?exam='.$exam->id) }}"
                                           class="btn btn-info btn-sm">Details </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>

@endsection
