@extends('layouts.backend.index')
@section('content')
    <div class="page-header">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('instructor.dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Exam</li>
        </ol>

        <h1 class="page-title">Take this exam</h1>

    </div>

    <div class="page-content">

        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ url('questions/store/') }}" id="courseForm">
                        @foreach($exam->questions as $question)
                            <div class="col-xs-12 form-group">
                                <div class="form-group">
                                    <h4>{{ $loop->index + 1 }}: {!! ($question->question) !!} </h4>
                                    <input type="hidden" name="" value="{{ $question->question }}">
                                    @foreach($question->choices as $key => $choice)
                                        <div>
                                            <input type="radio" name="choice"
                                                   value="choice_{{ $question->id.'_'.$key }}"> {{ $choice }}
                                        </div>
                                    @endforeach
                                </div>
                                <br>
                            </div>
                        @endforeach
                    </div>

                    <hr>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
                <!-- End Panel Basic -->
            </div>
        </div>

@endsection
