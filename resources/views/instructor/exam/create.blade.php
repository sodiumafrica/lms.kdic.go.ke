@extends('layouts.backend.index')
@section('content')
    <div class="page-header">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('instructor.dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ url('exams') }}">Exam</a></li>
            <li class="breadcrumb-item active">Add</li>
        </ol>
        <h1 class="page-title">Add Exam</h1>

    </div>

    <div class="page-content">

        <div class="card">
            <div class="card-body">

             <form method="POST" action="{{ url('exams') }}" id="courseForm">
                    {{ csrf_field() }}
                 <div class="row">
                     <div class="form-group col-md-8">
                    <label class="form-control-label">Name</label>
                    <input type="text" class="form-control" name="name" />
                   </div>
                 </div>

                    <hr>
                  <div class="form-group row">
                     <div class="col-md-4">
                        <button type="submit" class="btn btn-primary">Save</button>
                     </div>
                  </div>

             </form>
           </div>
        </div>
    </div>


        <!-- End Panel Basic -->

@endsection

