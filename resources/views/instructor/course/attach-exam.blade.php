<div class="modal fade examModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLongTitle">Attach Exam</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <label>Choose an exam to attach to this section</label>
                    <select class="form-control selExam">
                        <option value="">Choose an exam</option>
                        @if($exams->count())
                            @foreach($exams as $exam)
                                <option value="{{ $exam->id }}">{{ $exam->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>

                <a href="/exams" target="_blank">Manage examinations</a>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-success btnAddExam">Attach Exam</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('.btnAddExam').click(function() {
            $(this).html('Saving...').addClass('disabled')
            $.post('/attach-exam', {
                sectionID: examSection,
                examID: $('.selExam').val()
            }, function(res) {
                location.reload()
            })
        })
    })

</script>