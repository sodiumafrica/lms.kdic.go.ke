// TableRow.js

import React, {Component} from 'react';
import {Link, NavLink, Route} from "react-router-dom";

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

export default class SideItem extends Component {
    render() {
        return (
            <React.Fragment>
                <li className="site-menu-category">{this.props.section.title}</li>
                <li className="site-menu-item">

                    <ul>
                        {this.props.section.lessons.map((lesson, key) => {
                            return (
                                <NavLink to={base_url + "/" + lesson.url} style={{marginLeft: '1.7em'}}>
                                    <article>
                                        <i className={(lesson.completed == 'NO') ? 'fas fa-clock' : 'fas fa-check-circle'}></i>
                                        &nbsp;
                                        &nbsp;
                                        <span>{lesson.title}</span>
                                    </article>
                                </NavLink>
                            );
                        })}
                    </ul>

                </li>
            </React.Fragment>
        );
    }
}