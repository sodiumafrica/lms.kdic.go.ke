import React, {Component} from 'react';
import {Route} from "react-router-dom";

import SideItem from '../components/SideItem';

export default class SideBar extends Component {
    constructor(props) {
        super(props);
    }

    sideItem() {
        if (this.props.sections instanceof Array) {
            let self = this;
            return this.props.sections.map(function (section, i) {
                return <SideItem section={section} key={i} />;
            })
        }
    }

    render() {
        return (
            <div className="site-menubar-body">
                <ul className="site-menu">
                    {this.sideItem()}
                </ul>
            </div>
        );
    }
}

