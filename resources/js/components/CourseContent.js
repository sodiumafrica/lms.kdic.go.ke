import React, {Component} from 'react';
import {NavLink, Route} from "react-router-dom";

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';


export default class CourseContent extends Component {
    constructor(props) {
        super(props);
        this.state = {is_completed: null};
        console.log(this.props)
    }

    handleClick() {
        axios.get(site_url + '/update-lecture-status/' + this.props.course_id + '/' + this.props.lecture.lecture_quiz_id + '/' + !this.state.is_completed)
            .then(response => {
                this.setState({is_completed: !this.state.is_completed});
                // alert('Click on the Next Lesson button to proceed');
            })
            .catch(function (error) {
                console.log(error);
                return window.location.href = site_url + '/login';
            })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.lecture.lecture_quiz_id != this.props.lecture.lecture_quiz_id) {
            this.setState({is_completed: nextProps.lecture.completion_status});
        }
    }

    completedStatus() {

        if (this.state.is_completed == true) {
            return (
                <button is_completed={this.props.lecture.completion_status ? 1 : 0} className="btn btn-success btn-lg" onClick={this.handleClick.bind(this)}>
                    <FontAwesomeIcon icon="check"/>
                    &nbsp; You have taken this course
                </button>
            );
        } else if (this.state.is_completed == false) {
            return (
                <button is_completed={this.props.lecture.completion_status ? 1 : 0} className="btn btn-success" onClick={this.handleClick.bind(this)}>
                    Mark as completed
                </button>
            );
        }
    }

    lectureFile() {
        if (this.props.lecture.media_type == 'PICTURE') {
            return (
                <div className="col d-flex align-items-center justify-content-center">
                    <img src={this.props.lecture.media.path} class="img-fluid"/>
                </div>
            );
        } else if (this.props.lecture.media_type == 'VIDEO') {
            return (
                <div className="col d-flex align-items-center justify-content-center">
                    <video controls>
                        <source src={this.props.lecture.media.path} type="video/mp4"/>
                    </video>
                </div>
            );
        } else if (this.props.lecture.media_type == 'TEXT') {
            return (
                <div className="col mt-4">
                    <div dangerouslySetInnerHTML={{__html: this.props.lecture.contenttext}}/>
                </div>
            );
        } else if (this.props.lecture.media_type == 'AUDIO') {
            return (
                <div className="col d-flex align-items-center justify-content-center">
                    <audio controls>
                        <source src={this.props.lecture.media.path} type="audio/mpeg"/>
                    </audio>
                </div>
            );
        } else if (this.props.lecture.media_type == 'PDF') {
            return (
                <div className="col mt-4">
                    <iframe src={this.props.lecture.media.path} width="100%" height="600px"></iframe>
                </div>
            );
        }

    }

    next() {
        if (this.props.next) {
            return (
                <li className="page-item">
                    <NavLink className="page-link" to={this.props.next}>Next Lessonx <span aria-hidden="true">→</span></NavLink>
                </li>
            );
        }
        else {
            return (
                <li className="page-item disabled">
                    <a className="page-link" href="javascript:void(0)">Next Lessonxx <span aria-hidden="true">→</span></a>
                </li>
            );
        }
    }

    prev() {
        if (this.props.prev) {
            return (
                <li className="page-item">
                    <NavLink className="page-link" to={this.props.lecture.prev}><span aria-hidden="true">←</span> Previous Lesson</NavLink>
                </li>
            );
        }
        else {
            return (
                <li className="page-item disabled">
                    <a className="page-link" href="javascript:void(0)">
                        <span aria-hidden="true">←</span> Previous Lesson</a>
                </li>
            );
        }
    }

    descriptionTemplate() {
        return (
            <div dangerouslySetInnerHTML={{__html: this.props.lecture.description}}/>
        );
    }

    render() {
        return (
            <div className="page-content container-fluid">

                <div className="row">
                    <div className="col-xl-8 col-md-12 col-sm-12 col-12">
                        <h1 className="page-title">Title: {this.props.lecture.title}</h1>
                    </div>
                    <div className="col-xl-4 col-md-6 col-sm-6 col-6">
                        <ul className="pagination">
                            {this.prev()}
                            {this.next()}
                        </ul>
                    </div>
                    <div className="col-md-12 mt-3 mb-3">
                        <div className="alert alert-info" role="alert" style={{display: (this.props.lecture.completed == 'NO') ? 'none' : 'block'}}>
                            You will earn <b>10 marks</b> once you finish this lesson
                        </div>
                        <div className="alert alert-success" role="alert" style={{display: (this.props.lecture.completed == 'YES') ? 'block' : 'none'}}>
                            You have completed this lesson! <b className="float-right">10 marks</b>
                        </div>

                        <div className="card" style={{display: (!this.props.lecture.description) ? 'none' : 'block'}}>
                            <div className="card-header">
                                Lesson Description
                            </div>
                            <div className="card-body">
                                {this.descriptionTemplate()}
                            </div>
                        </div>
                    </div>

                    <div className="col-md-12">
                        <div className="card">
                            <div className="card-header">
                                Lesson Content
                            </div>
                            <div className="card-body">
                                {this.lectureFile()}
                            </div>
                        </div>
                    </div>

                    <div className="col-md-12 mt-3 mb-5">
                        {this.completedStatus()}
                    </div>

                </div>


            </div>
        );
    }
}

