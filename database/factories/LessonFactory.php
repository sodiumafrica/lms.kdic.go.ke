<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Lesson::class, function (Faker $faker) {
    return [
        'section_id'  => NULL,
        'title'       => $faker->sentence(rand(2, 7)),
        'description' => $faker->sentence(rand(10, 60)),
        'contenttext' => implode(' ', $faker->sentences(rand(10, 60))),
        'media'       => NULL,
        'media_type'  => NULL,
        'publish'     => 1,
        'resources'   => NULL,
    ];
});
