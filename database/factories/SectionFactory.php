<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\CourseSection::class, function (Faker $faker) {
    return [
        'title'     => 'Section ' . rand(3, 7),
        'course_id' => NULL,
        'createdOn' => date("Y-m-d H:i:s"),
        'updatedOn' => date("Y-m-d H:i:s"),
    ];


});
