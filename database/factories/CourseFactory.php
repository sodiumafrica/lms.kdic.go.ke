<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Course::class, function (Faker $faker) {
    return [
        'course_title'         => 'Deposit Insurance Training for Bankers',
        'instructor_id'        => 1,
        'category_id'          => 1,
        'instruction_level_id' => 1,
        'duration'             => '2 days',
        'price'                => 0,
        'strike_out_price'     => 0,
        'course_slug'          => str_slug('Deposit Insurance Training for Bankers', '-'),
        'course_image'         => 'placeholder.png',
        'thumb_image'          => 'placeholder.png',
        'keywords'             => implode(',', $faker->words(rand(3, 10))),
        'overview'             => implode(' ', $faker->sentences(rand(3, 10))),
        'is_active'            => 1
    ];
});
