<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Question::class, function (Faker $faker) {

    $choices = ['Sample answer ' . rand(1, 5), 'Sample answer ' . rand(1, 5), 'Sample answer ' . rand(1, 5), 'Sample answer ' . rand(1, 5)];
    $answer = $choices[rand(0, (count($choices)) - 1)];
    return [
        'question' => 'Is this a sample question?',
        'score'    => 2,
        'choices'  => $choices,
        'answer'   => $answer,
        'reasons'   => 'Sample reason for answer ' . rand(1, 5)
    ];
});
