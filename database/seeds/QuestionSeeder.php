<?php

use Illuminate\Database\Seeder;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('exams')->get()->each(function ($exam) {
            factory(\App\Models\Question::class, rand(4, 8))->create([
                'exam_id' => $exam->id
            ]);
        });
    }
}
