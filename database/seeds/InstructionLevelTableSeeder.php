<?php

use Illuminate\Database\Seeder;
use App\Models\InstructionLevel;

class InstructionLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $is_exist = InstructionLevel::all();

        if (!$is_exist->count()) {
            $instruction_levels = new InstructionLevel();
            $instruction_levels->level = 'Simple Reading';
            $instruction_levels->save();

            $instruction_levels = new InstructionLevel();
            $instruction_levels->level = 'Intermediate Reading';
            $instruction_levels->save();

            $instruction_levels = new InstructionLevel();
            $instruction_levels->level = 'Advanced Reading';
            $instruction_levels->save();

        }
    }
}
