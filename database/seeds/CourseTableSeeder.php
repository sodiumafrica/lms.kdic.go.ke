<?php

use App\Models\Course;
use App\Models\CourseTaken;
use App\Models\Credit;
use App\Models\Transaction;
use Illuminate\Database\Seeder;

class CourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # Make 1 course
        factory(Course::class)->create();

        $courses = Course::get();

//        echo json_encode($courses, JSON_PRETTY_PRINT); exit;


        $courses->each(function ($course) {

            # Make sections
            factory(\App\Models\CourseSection::class, 3)->create([
                'course_id' => $course->id
            ]);

            $sections = \App\Models\CourseSection::where('course_id', $course->id)->get();
            $sections->each(function ($section) {

                # Make lessons for each section
                factory(\App\Models\Lesson::class)->create([
                    'title'      => 'Text Lesson',
                    'section_id' => $section->section_id,
                    'media_type' => 'TEXT',
                ]);
                factory(\App\Models\Lesson::class)->create([
                    'title'       => 'Video Lesson',
                    'section_id'  => $section->section_id,
                    'description' => '',
                    'contenttext' => '',
                    'media_type'  => 'VIDEO',
                    'media'       => 1,
                    'resources'   => [1]

                ]);
                factory(\App\Models\Lesson::class)->create([
                    'title'       => 'Audio Lesson',
                    'section_id'  => $section->section_id,
                    'description' => '',
                    'contenttext' => '',
                    'media_type'  => 'AUDIO',
                    'media'       => 2,
                    'resources'   => [2]

                ]);
                factory(\App\Models\Lesson::class)->create([
                    'title'       => 'Picture Lesson',
                    'section_id'  => $section->section_id,
                    'description' => '',
                    'contenttext' => '',
                    'media_type'  => 'PICTURE',
                    'media'       => 3,
                    'resources'   => [3]

                ]);
                factory(\App\Models\Lesson::class)->create([
                    'title'       => 'PDF Lesson',
                    'section_id'  => $section->section_id,
                    'description' => '',
                    'contenttext' => '',
                    'media_type'  => 'PDF',
                    'media'       => 4,
                    'resources'   => [4]

                ]);
            });

        });

        # Setup media
        # Video
        \App\Models\File::create([
            'file_name'      => 'Test Video',
            'file_title'     => 'Test Video',
            'path'           => 'storage/sample_data/video.mp4',
            'file_type'      => 'mp4',
            'file_extension' => 'mp4',
            'file_size'      => '4113874',
            'duration'       => '00:00:00',
            'file_tag'       => 'curriculum',
            'uploader_id'    => 1,
            'processed'      => 1,
            'created_at'     => now(),
            'updated_at'     => now()
        ]);

        # Audio
        \App\Models\File::create([
            'file_name'      => 'Test Audio',
            'file_title'     => 'Test Audio',
            'path'           => 'storage/sample_data/audio.mp3',
            'file_type'      => 'mp3',
            'file_extension' => 'mp3',
            'file_size'      => '4113874',
            'duration'       => '00:00:00',
            'file_tag'       => 'curriculum',
            'uploader_id'    => 1,
            'processed'      => 1,
            'created_at'     => now(),
            'updated_at'     => now()
        ]);

        # Picture
        \App\Models\File::create([
            'file_name'      => 'Test Picture',
            'file_title'     => 'Test Picture',
            'path'           => 'storage/sample_data/picture.jpg',
            'file_type'      => 'pdf',
            'file_extension' => 'pdf',
            'file_size'      => '4113874',
            'duration'       => '00:00:00',
            'file_tag'       => 'curriculum',
            'uploader_id'    => 1,
            'processed'      => 1,
            'created_at'     => now(),
            'updated_at'     => now()
        ]);

        # PDF
        \App\Models\File::create([
            'file_name'      => 'Test PDF',
            'file_title'     => 'Test PDF',
            'path'           => 'storage/sample_data/pdf.pdf',
            'file_type'      => 'pdf',
            'file_extension' => 'pdf',
            'file_size'      => '4113874',
            'duration'       => '00:00:00',
            'file_tag'       => 'curriculum',
            'uploader_id'    => 1,
            'processed'      => 1,
            'created_at'     => now(),
            'updated_at'     => now()
        ]);

        # To continue

        $user_id = $course_id = 1;
        $course = Course::find($course_id);
        $instructor_id = $course->instructor_id;

        $is_course_taken_exist = CourseTaken::all();
        if (!$is_course_taken_exist->count()) {
            $course_taken = new CourseTaken();
            $course_taken->user_id = $user_id;
            $course_taken->course_id = $course_id;
            $course_taken->save();
        }

        $is_transaction_taken_exist = Transaction::all();
        if (!$is_transaction_taken_exist->count()) {
            $transaction = new Transaction();
            $transaction->user_id = $user_id;
            $transaction->course_id = $course_id;
            $transaction->amount = 0.00;
            $transaction->status = 'completed';
            $transaction->payment_method = 'paypal_express_checkout';
            $transaction->order_details = '{"TOKEN":"success","status":"succeeded","Timestamp":1561787415,"ACK":"Success"}';
            $transaction->save();
        }

        $is_credits_taken_exist = Credit::all();
        if (!$is_credits_taken_exist->count()) {
            $credits = new Credit();
            $credits->transaction_id = $transaction->id;
            $credits->instructor_id = $instructor_id;
            $credits->user_id = $user_id;
            $credits->course_id = $course_id;
            $credits->credit = $course->price;
            $credits->credits_for = 1;
            $credits->is_admin = 0;

            $credits = new Credit();
            $credits->transaction_id = $transaction->id;
            $credits->instructor_id = 0;
            $credits->user_id = $user_id;
            $credits->course_id = $course_id;
            $credits->credit = $course->price;
            $credits->credits_for = 2;
            $credits->is_admin = 1;

            $credits->save();
        }
    }
}
