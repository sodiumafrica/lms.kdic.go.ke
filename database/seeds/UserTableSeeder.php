<?php

use App\Models\Instructor;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        $role_student = Role::where('name', 'student')->first();
        $role_instructor = Role::where('name', 'instructor')->first();
        $role_admin = Role::where('name', 'admin')->first();

        $is_exist = User::all();

        if (!$is_exist->count()) {
            $student = new User();
            $student->first_name = 'Admin';
            $student->last_name = 'Account';
            $student->email = 'admin@gmail.com';
            $student->password = bcrypt('root');
            $student->is_active = 1;
            $student->save();
            $student->roles()->attach($role_admin);

            $admin = new User();
            $admin->first_name = 'Student';
            $admin->last_name = 'Account';
            $admin->email = 'student@gmail.com';
            $admin->password = bcrypt('root');
            $admin->is_active = 1;
            $admin->save();
            $admin->roles()->attach($role_student);


            //import instructors
            $instructor_user = new User();
            $instructor_user->first_name = 'Instructor';
            $instructor_user->last_name = 'Account';
            $instructor_user->email = 'instructor@gmail.com';
            $instructor_user->password = bcrypt('root');
            $instructor_user->is_active = 1;
            $instructor_user->save();
            $instructor_user->roles()->attach($role_instructor);

            $instructor = new Instructor();
            $instructor->user_id = $instructor_user->id;
            $instructor->first_name = 'Instructor';
            $instructor->last_name = 'Account';
            $instructor->instructor_slug = 'instructor-account';
            $instructor->contact_email = 'instructor@gmail.com';
            $instructor->telephone = '0700 123 456';
            $instructor->mobile = '0700 123 456';
            $instructor->paypal_id = 'instructor@gmail.com';
            $instructor->biography = '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>';
            $instructor->instructor_image = 'frontend/img/avatar.jpg';
            $instructor->save();
            $instructor_user->roles()->attach($role_instructor);


        }
    }
}