<?php

use Illuminate\Database\Seeder;

class ExamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('curriculum_sections')->get()->each(function ($row) {
            factory(\App\Models\Exam::class)->create([
                'section_id' => $row->section_id
            ]);
        });
    }
}
