<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $is_exist = Category::all();

        if (!$is_exist->count()) {
            $category = new Category();
            $category->name = 'Banking & Finance';
            $category->slug = 'banking-finance';
            $category->icon_class = 'fa-money';
            $category->is_active = 1;
            $category->save();


        }
    }
}
