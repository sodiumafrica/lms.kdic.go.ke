const mix = require('laravel-mix');

mix.react('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

mix.browserSync({
    files: ['app/**', 'resources/**', 'routes/**'],
    proxy: '127.0.0.1:8000'
});

