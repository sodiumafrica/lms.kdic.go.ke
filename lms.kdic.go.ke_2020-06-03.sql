# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: kdic.tdm.co.ke (MySQL 5.5.5-10.1.44-MariaDB-0ubuntu0.18.04.1)
# Database: lms.kdic.go.ke
# Generation Time: 2020-06-03 16:45:16 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table blogs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blogs`;

CREATE TABLE `blogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blog_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `name`, `slug`, `icon_class`, `is_active`, `created_at`, `updated_at`)
VALUES
	(1,'Banking & Finance','banking-finance','fa-money',1,'2020-01-08 10:18:09','2020-01-08 10:18:09');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table course_files
# ------------------------------------------------------------

DROP TABLE IF EXISTS `course_files`;

CREATE TABLE `course_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_type` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_extension` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_tag` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `uploader_id` int(11) NOT NULL,
  `processed` int(11) NOT NULL DEFAULT '1' COMMENT '0-not processed,1-processed',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `course_files` WRITE;
/*!40000 ALTER TABLE `course_files` DISABLE KEYS */;

INSERT INTO `course_files` (`id`, `file_name`, `file_title`, `path`, `file_type`, `file_extension`, `file_size`, `duration`, `file_tag`, `uploader_id`, `processed`, `created_at`, `updated_at`)
VALUES
	(1,'Test Video','Test Video','storage/sample_data/video.mp4','mp4','mp4','4113874','00:00:00','curriculum',1,1,2020,2020),
	(2,'Test Audio','Test Audio','storage/sample_data/audio.mp3','mp3','mp3','4113874','00:00:00','curriculum',1,1,2020,2020),
	(3,'Test Picture','Test Picture','storage/sample_data/picture.jpg','pdf','pdf','4113874','00:00:00','curriculum',1,1,2020,2020),
	(4,'Test PDF','Test PDF','storage/sample_data/pdf.pdf','pdf','pdf','4113874','00:00:00','curriculum',1,1,2020,2020),
	(5,'zqYOxGsarCdW2nt9.jpg','zqYOxGsarCdW2nt9.jpg','course/1/zqYOxGsarCdW2nt9.jpg','image/jpeg','jpg','0',NULL,'lesson_picture',3,1,0,0),
	(6,'abfYwVfjVGjOVhuU.jpg','abfYwVfjVGjOVhuU.jpg','course/1/abfYwVfjVGjOVhuU.jpg','image/jpeg','jpg','0',NULL,'lesson_picture',3,1,0,0),
	(7,'gXTvqvaPGEIgt0Uv.jpg','gXTvqvaPGEIgt0Uv.jpg','course/1/gXTvqvaPGEIgt0Uv.jpg','image/jpeg','jpg','0',NULL,'lesson_picture',3,1,0,0);

/*!40000 ALTER TABLE `course_files` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table course_progress
# ------------------------------------------------------------

DROP TABLE IF EXISTS `course_progress`;

CREATE TABLE `course_progress` (
  `progress_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `lecture_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0-incomplete,1-complete',
  `score` int(11) NOT NULL DEFAULT '10',
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`progress_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `course_progress` WRITE;
/*!40000 ALTER TABLE `course_progress` DISABLE KEYS */;

INSERT INTO `course_progress` (`progress_id`, `user_id`, `course_id`, `lecture_id`, `status`, `score`, `created_at`, `modified_at`)
VALUES
	(1,2,1,16,0,10,'0000-00-00 00:00:00','2020-01-18 02:32:15'),
	(2,2,1,16,1,10,'0000-00-00 00:00:00','2020-01-18 02:32:27'),
	(3,2,1,18,0,10,'0000-00-00 00:00:00','2020-01-18 02:32:31'),
	(4,2,1,18,1,10,'0000-00-00 00:00:00','2020-01-18 02:32:34'),
	(5,2,1,19,0,10,'0000-00-00 00:00:00','2020-01-18 02:32:37'),
	(6,2,1,19,1,10,'0000-00-00 00:00:00','2020-01-18 02:32:40'),
	(7,2,1,20,0,10,'0000-00-00 00:00:00','2020-01-18 02:32:43'),
	(8,2,1,20,1,10,'0000-00-00 00:00:00','2020-01-18 02:32:46'),
	(9,2,1,26,0,10,'0000-00-00 00:00:00','2020-01-18 02:32:52'),
	(10,2,1,26,1,10,'0000-00-00 00:00:00','2020-01-18 02:32:59'),
	(11,2,1,28,0,10,'0000-00-00 00:00:00','2020-01-18 02:33:02'),
	(12,2,1,28,1,10,'0000-00-00 00:00:00','2020-01-18 02:33:05'),
	(13,2,1,29,0,10,'0000-00-00 00:00:00','2020-01-18 02:33:12'),
	(14,2,1,29,1,10,'0000-00-00 00:00:00','2020-01-18 02:33:55'),
	(15,2,1,30,0,10,'0000-00-00 00:00:00','2020-01-18 02:33:59'),
	(16,2,1,31,0,10,'0000-00-00 00:00:00','2020-01-20 04:20:15'),
	(17,2,1,30,1,10,'0000-00-00 00:00:00','2020-01-20 04:21:15'),
	(18,2,1,31,1,10,'0000-00-00 00:00:00','2020-01-20 04:21:22'),
	(19,8,1,31,0,10,'0000-00-00 00:00:00','2020-01-20 04:23:05'),
	(20,8,1,31,1,10,'0000-00-00 00:00:00','2020-01-20 04:23:07'),
	(21,7,1,16,0,10,'0000-00-00 00:00:00','2020-01-21 09:13:49'),
	(22,7,1,19,0,10,'0000-00-00 00:00:00','2020-01-21 09:13:54'),
	(23,7,1,19,1,10,'0000-00-00 00:00:00','2020-01-21 09:14:02'),
	(24,7,1,20,0,10,'0000-00-00 00:00:00','2020-01-21 09:14:08'),
	(25,7,1,20,1,10,'0000-00-00 00:00:00','2020-01-21 09:14:15'),
	(26,7,1,26,0,10,'0000-00-00 00:00:00','2020-01-21 09:14:23'),
	(27,7,1,26,1,10,'0000-00-00 00:00:00','2020-01-21 09:14:37'),
	(28,7,1,28,0,10,'0000-00-00 00:00:00','2020-01-21 09:14:43'),
	(29,7,1,28,1,10,'0000-00-00 00:00:00','2020-01-21 09:14:48'),
	(30,7,1,16,1,10,'0000-00-00 00:00:00','2020-02-24 15:09:10'),
	(31,7,1,18,0,10,'0000-00-00 00:00:00','2020-02-24 15:09:17'),
	(32,7,1,18,1,10,'0000-00-00 00:00:00','2020-02-24 15:09:29'),
	(33,9,1,16,0,10,'0000-00-00 00:00:00','2020-02-24 15:10:40'),
	(34,7,1,29,0,10,'0000-00-00 00:00:00','2020-03-20 07:39:39'),
	(35,6,1,16,0,10,'0000-00-00 00:00:00','2020-03-20 09:24:44'),
	(36,6,1,16,1,10,'0000-00-00 00:00:00','2020-03-20 09:25:40'),
	(37,6,1,18,0,10,'0000-00-00 00:00:00','2020-03-20 09:25:48'),
	(38,6,1,28,0,10,'0000-00-00 00:00:00','2020-03-20 09:26:22'),
	(39,6,1,19,0,10,'0000-00-00 00:00:00','2020-03-20 09:26:55'),
	(40,6,1,20,0,10,'0000-00-00 00:00:00','2020-03-20 09:27:06'),
	(41,6,1,26,0,10,'0000-00-00 00:00:00','2020-03-20 09:27:18'),
	(42,10,1,29,0,10,'0000-00-00 00:00:00','2020-03-20 10:04:37'),
	(43,10,1,19,0,10,'0000-00-00 00:00:00','2020-03-20 10:05:09'),
	(44,10,1,16,0,10,'0000-00-00 00:00:00','2020-03-20 10:05:11'),
	(45,10,1,28,0,10,'0000-00-00 00:00:00','2020-03-20 10:05:32'),
	(46,10,1,28,1,10,'0000-00-00 00:00:00','2020-03-20 10:05:37'),
	(47,10,1,20,0,10,'0000-00-00 00:00:00','2020-03-20 10:05:58'),
	(48,10,1,26,0,10,'0000-00-00 00:00:00','2020-03-20 10:06:02'),
	(49,10,1,30,0,10,'0000-00-00 00:00:00','2020-03-20 10:06:14'),
	(50,11,1,31,0,10,'0000-00-00 00:00:00','2020-03-20 10:15:05'),
	(51,11,1,31,1,10,'0000-00-00 00:00:00','2020-03-20 10:15:08'),
	(52,3,1,16,0,10,'0000-00-00 00:00:00','2020-03-23 15:18:35'),
	(53,3,1,16,1,10,'0000-00-00 00:00:00','2020-03-23 15:18:45'),
	(54,3,1,18,0,10,'0000-00-00 00:00:00','2020-03-23 15:18:50'),
	(55,3,1,26,0,10,'0000-00-00 00:00:00','2020-03-23 15:37:43'),
	(56,3,1,20,0,10,'0000-00-00 00:00:00','2020-03-23 15:37:52'),
	(57,3,1,19,0,10,'0000-00-00 00:00:00','2020-03-23 16:01:42'),
	(58,11,1,28,0,10,'0000-00-00 00:00:00','2020-03-23 16:29:52'),
	(59,11,1,16,0,10,'0000-00-00 00:00:00','2020-03-23 16:51:59'),
	(60,11,1,18,0,10,'0000-00-00 00:00:00','2020-03-23 17:11:09'),
	(61,11,1,35,0,10,'0000-00-00 00:00:00','2020-03-23 17:11:22'),
	(62,11,1,37,0,10,'0000-00-00 00:00:00','2020-03-23 17:11:28'),
	(63,11,1,38,0,10,'0000-00-00 00:00:00','2020-03-23 17:12:47'),
	(64,11,1,40,0,10,'0000-00-00 00:00:00','2020-03-23 18:11:38'),
	(65,11,1,43,0,10,'0000-00-00 00:00:00','2020-03-23 18:56:27'),
	(66,11,1,44,0,10,'0000-00-00 00:00:00','2020-03-23 18:56:31'),
	(67,11,1,45,0,10,'0000-00-00 00:00:00','2020-03-23 18:56:39'),
	(68,12,1,16,0,10,'0000-00-00 00:00:00','2020-03-24 12:35:10'),
	(69,12,1,18,0,10,'0000-00-00 00:00:00','2020-03-24 12:35:26'),
	(70,12,1,37,0,10,'0000-00-00 00:00:00','2020-03-24 12:35:33'),
	(71,12,1,38,0,10,'0000-00-00 00:00:00','2020-03-24 12:35:39'),
	(72,12,1,43,0,10,'0000-00-00 00:00:00','2020-03-24 12:35:47'),
	(73,12,1,47,0,10,'0000-00-00 00:00:00','2020-03-24 12:35:56'),
	(74,12,1,46,0,10,'0000-00-00 00:00:00','2020-03-24 12:37:49'),
	(75,12,1,42,0,10,'0000-00-00 00:00:00','2020-03-24 12:37:57'),
	(76,12,1,44,0,10,'0000-00-00 00:00:00','2020-03-24 12:38:07'),
	(77,11,1,42,0,10,'0000-00-00 00:00:00','2020-04-16 16:20:46'),
	(78,11,1,48,0,10,'0000-00-00 00:00:00','2020-04-16 16:49:38'),
	(79,11,1,53,0,10,'0000-00-00 00:00:00','2020-04-16 16:53:05'),
	(80,11,1,47,0,10,'0000-00-00 00:00:00','2020-04-16 16:53:09'),
	(81,11,1,67,0,10,'0000-00-00 00:00:00','2020-04-16 17:47:42'),
	(82,7,1,61,0,10,'0000-00-00 00:00:00','2020-04-28 11:35:46'),
	(83,7,1,61,1,10,'0000-00-00 00:00:00','2020-04-28 11:36:00'),
	(84,7,1,62,0,10,'0000-00-00 00:00:00','2020-04-28 11:36:19'),
	(85,7,1,62,1,10,'0000-00-00 00:00:00','2020-04-28 11:36:29'),
	(86,7,1,63,0,10,'0000-00-00 00:00:00','2020-04-28 11:36:41'),
	(87,7,1,59,0,10,'0000-00-00 00:00:00','2020-04-28 11:36:52'),
	(88,7,1,40,0,10,'0000-00-00 00:00:00','2020-04-28 11:38:21'),
	(89,7,1,35,0,10,'0000-00-00 00:00:00','2020-04-28 11:38:32'),
	(90,7,1,35,1,10,'0000-00-00 00:00:00','2020-04-28 11:39:09'),
	(91,7,1,37,0,10,'0000-00-00 00:00:00','2020-04-28 11:39:15'),
	(92,7,1,37,1,10,'0000-00-00 00:00:00','2020-04-28 11:39:20'),
	(93,7,1,38,0,10,'0000-00-00 00:00:00','2020-04-28 11:39:26'),
	(94,7,1,38,1,10,'0000-00-00 00:00:00','2020-04-28 11:39:32'),
	(95,7,1,39,0,10,'0000-00-00 00:00:00','2020-04-28 11:39:37'),
	(96,7,1,39,1,10,'0000-00-00 00:00:00','2020-04-28 11:39:45'),
	(97,7,1,40,1,10,'0000-00-00 00:00:00','2020-04-28 11:39:55'),
	(98,13,1,16,0,10,'0000-00-00 00:00:00','2020-05-12 10:43:44'),
	(99,13,1,16,1,10,'0000-00-00 00:00:00','2020-05-12 10:43:57'),
	(100,13,1,18,0,10,'0000-00-00 00:00:00','2020-05-12 10:44:12'),
	(101,13,1,18,1,10,'0000-00-00 00:00:00','2020-05-12 10:45:29'),
	(102,13,1,28,0,10,'0000-00-00 00:00:00','2020-05-12 10:45:35'),
	(103,13,1,28,1,10,'0000-00-00 00:00:00','2020-05-12 10:46:04'),
	(104,13,1,35,0,10,'0000-00-00 00:00:00','2020-05-12 10:46:14'),
	(105,13,1,35,1,10,'0000-00-00 00:00:00','2020-05-12 10:47:10'),
	(106,13,1,37,0,10,'0000-00-00 00:00:00','2020-05-12 10:47:57'),
	(107,13,1,37,1,10,'0000-00-00 00:00:00','2020-05-12 10:48:57'),
	(108,13,1,40,0,10,'0000-00-00 00:00:00','2020-05-12 10:49:12'),
	(109,13,1,38,0,10,'0000-00-00 00:00:00','2020-05-12 10:55:33'),
	(110,13,1,38,1,10,'0000-00-00 00:00:00','2020-05-12 10:55:59'),
	(111,13,1,39,0,10,'0000-00-00 00:00:00','2020-05-12 10:56:08'),
	(112,13,1,39,1,10,'0000-00-00 00:00:00','2020-05-12 10:56:14'),
	(113,13,1,40,1,10,'0000-00-00 00:00:00','2020-05-12 10:56:25'),
	(114,13,1,42,0,10,'0000-00-00 00:00:00','2020-05-12 11:03:26'),
	(115,13,1,74,0,10,'0000-00-00 00:00:00','2020-05-12 11:03:49'),
	(116,13,1,42,1,10,'0000-00-00 00:00:00','2020-05-12 11:05:52'),
	(117,13,1,43,0,10,'0000-00-00 00:00:00','2020-05-12 11:06:03'),
	(118,13,1,43,1,10,'0000-00-00 00:00:00','2020-05-12 11:06:11'),
	(119,13,1,44,0,10,'0000-00-00 00:00:00','2020-05-12 11:06:14'),
	(120,13,1,44,1,10,'0000-00-00 00:00:00','2020-05-12 11:06:37'),
	(121,13,1,45,0,10,'0000-00-00 00:00:00','2020-05-12 11:07:12'),
	(122,13,1,45,1,10,'0000-00-00 00:00:00','2020-05-12 11:07:22'),
	(123,13,1,46,0,10,'0000-00-00 00:00:00','2020-05-12 11:08:17'),
	(124,13,1,46,1,10,'0000-00-00 00:00:00','2020-05-12 11:08:26'),
	(125,13,1,47,0,10,'0000-00-00 00:00:00','2020-05-12 11:08:38'),
	(126,13,1,47,1,10,'0000-00-00 00:00:00','2020-05-12 11:08:57'),
	(127,13,1,48,0,10,'0000-00-00 00:00:00','2020-05-12 11:09:03'),
	(128,13,1,48,1,10,'0000-00-00 00:00:00','2020-05-12 11:11:04'),
	(129,13,1,49,0,10,'0000-00-00 00:00:00','2020-05-12 11:11:11'),
	(130,13,1,49,1,10,'0000-00-00 00:00:00','2020-05-12 11:11:43'),
	(131,13,1,50,0,10,'0000-00-00 00:00:00','2020-05-12 11:11:48'),
	(132,13,1,50,1,10,'0000-00-00 00:00:00','2020-05-12 11:12:07'),
	(133,13,1,51,0,10,'0000-00-00 00:00:00','2020-05-12 11:12:28'),
	(134,13,1,51,1,10,'0000-00-00 00:00:00','2020-05-12 11:12:49'),
	(135,13,1,52,0,10,'0000-00-00 00:00:00','2020-05-12 11:12:58'),
	(136,13,1,52,1,10,'0000-00-00 00:00:00','2020-05-12 11:13:15'),
	(137,13,1,53,0,10,'0000-00-00 00:00:00','2020-05-12 11:13:19'),
	(138,13,1,53,1,10,'0000-00-00 00:00:00','2020-05-12 11:13:55'),
	(139,13,1,54,0,10,'0000-00-00 00:00:00','2020-05-12 11:13:59'),
	(140,13,1,54,1,10,'0000-00-00 00:00:00','2020-05-12 11:32:47'),
	(141,13,1,55,0,10,'0000-00-00 00:00:00','2020-05-12 11:33:01'),
	(142,13,1,55,1,10,'0000-00-00 00:00:00','2020-05-12 11:33:15'),
	(143,13,1,56,0,10,'0000-00-00 00:00:00','2020-05-12 11:33:23'),
	(144,13,1,56,1,10,'0000-00-00 00:00:00','2020-05-12 11:33:33'),
	(145,13,1,57,0,10,'0000-00-00 00:00:00','2020-05-12 11:33:43'),
	(146,13,1,57,1,10,'0000-00-00 00:00:00','2020-05-12 11:33:48'),
	(147,13,1,58,0,10,'0000-00-00 00:00:00','2020-05-12 11:33:59'),
	(148,13,1,58,1,10,'0000-00-00 00:00:00','2020-05-12 11:34:08'),
	(149,13,1,59,0,10,'0000-00-00 00:00:00','2020-05-12 11:34:12'),
	(150,13,1,59,1,10,'0000-00-00 00:00:00','2020-05-12 11:34:26'),
	(151,13,1,60,0,10,'0000-00-00 00:00:00','2020-05-12 11:34:34'),
	(152,13,1,60,1,10,'0000-00-00 00:00:00','2020-05-12 11:35:06'),
	(153,13,1,61,0,10,'0000-00-00 00:00:00','2020-05-12 11:35:13'),
	(154,13,1,61,1,10,'0000-00-00 00:00:00','2020-05-12 11:37:01'),
	(155,13,1,62,0,10,'0000-00-00 00:00:00','2020-05-12 11:37:31'),
	(156,13,1,62,1,10,'0000-00-00 00:00:00','2020-05-12 11:37:47'),
	(157,13,1,63,0,10,'0000-00-00 00:00:00','2020-05-12 11:38:07'),
	(158,13,1,63,1,10,'0000-00-00 00:00:00','2020-05-12 11:38:15'),
	(159,13,1,64,0,10,'0000-00-00 00:00:00','2020-05-12 11:38:29'),
	(160,13,1,64,1,10,'0000-00-00 00:00:00','2020-05-12 11:38:46'),
	(161,13,1,65,0,10,'0000-00-00 00:00:00','2020-05-12 11:39:40'),
	(162,13,1,65,1,10,'0000-00-00 00:00:00','2020-05-12 11:39:55'),
	(163,13,1,66,0,10,'0000-00-00 00:00:00','2020-05-12 11:40:11'),
	(164,13,1,66,1,10,'0000-00-00 00:00:00','2020-05-12 11:40:21'),
	(165,13,1,67,0,10,'0000-00-00 00:00:00','2020-05-12 11:40:34'),
	(166,13,1,67,1,10,'0000-00-00 00:00:00','2020-05-12 11:41:02'),
	(167,13,1,68,0,10,'0000-00-00 00:00:00','2020-05-12 12:42:33'),
	(168,13,1,68,1,10,'0000-00-00 00:00:00','2020-05-12 12:44:41'),
	(169,13,1,69,0,10,'0000-00-00 00:00:00','2020-05-12 12:44:47'),
	(170,13,1,69,1,10,'0000-00-00 00:00:00','2020-05-12 12:45:18'),
	(171,13,1,70,0,10,'0000-00-00 00:00:00','2020-05-12 12:45:49'),
	(172,13,1,70,1,10,'0000-00-00 00:00:00','2020-05-12 12:46:01'),
	(173,13,1,71,0,10,'0000-00-00 00:00:00','2020-05-12 12:46:15'),
	(174,13,1,71,1,10,'0000-00-00 00:00:00','2020-05-12 12:46:23'),
	(175,13,1,72,0,10,'0000-00-00 00:00:00','2020-05-12 12:47:08'),
	(176,13,1,72,1,10,'0000-00-00 00:00:00','2020-05-12 12:47:20'),
	(177,13,1,73,0,10,'0000-00-00 00:00:00','2020-05-12 12:47:29'),
	(178,13,1,73,1,10,'0000-00-00 00:00:00','2020-05-12 12:59:40'),
	(179,13,1,74,1,10,'0000-00-00 00:00:00','2020-05-12 13:00:11'),
	(180,2,1,37,0,10,'0000-00-00 00:00:00','2020-06-03 16:14:20'),
	(181,2,1,38,0,10,'0000-00-00 00:00:00','2020-06-03 16:41:11'),
	(182,2,1,39,0,10,'0000-00-00 00:00:00','2020-06-03 16:41:21'),
	(183,2,1,40,0,10,'0000-00-00 00:00:00','2020-06-03 16:41:26');

/*!40000 ALTER TABLE `course_progress` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table course_ratings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `course_ratings`;

CREATE TABLE `course_ratings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `rating` decimal(8,1) NOT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table course_taken
# ------------------------------------------------------------

DROP TABLE IF EXISTS `course_taken`;

CREATE TABLE `course_taken` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `course_taken` WRITE;
/*!40000 ALTER TABLE `course_taken` DISABLE KEYS */;

INSERT INTO `course_taken` (`id`, `user_id`, `course_id`, `created_at`, `updated_at`)
VALUES
	(1,1,1,'2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(2,2,1,'2020-01-08 10:20:40','2020-01-08 10:20:40'),
	(3,3,1,'2020-01-08 11:55:34','2020-01-08 11:55:34'),
	(4,4,1,'2020-01-08 17:58:32','2020-01-08 17:58:32'),
	(5,5,1,'2020-01-17 07:02:21','2020-01-17 07:02:21'),
	(6,6,1,'2020-01-17 07:02:29','2020-01-17 07:02:29'),
	(7,7,1,'2020-01-17 13:15:22','2020-01-17 13:15:22'),
	(8,8,1,'2020-01-20 04:22:54','2020-01-20 04:22:54'),
	(9,9,1,'2020-02-24 15:10:29','2020-02-24 15:10:29'),
	(10,10,1,'2020-03-20 10:04:23','2020-03-20 10:04:23'),
	(11,11,1,'2020-03-20 10:11:36','2020-03-20 10:11:36'),
	(12,12,1,'2020-03-24 12:35:00','2020-03-24 12:35:00'),
	(13,13,1,'2020-05-12 10:42:45','2020-05-12 10:42:45');

/*!40000 ALTER TABLE `course_taken` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table course_videos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `course_videos`;

CREATE TABLE `course_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_title` text COLLATE utf8mb4_unicode_ci,
  `video_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_type` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_name` text COLLATE utf8mb4_unicode_ci,
  `video_tag` text COLLATE utf8mb4_unicode_ci,
  `uploader_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `processed` int(11) NOT NULL DEFAULT '1' COMMENT '0-not processed,1-processed',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `course_videos` WRITE;
/*!40000 ALTER TABLE `course_videos` DISABLE KEYS */;

INSERT INTO `course_videos` (`id`, `video_title`, `video_name`, `video_type`, `duration`, `image_name`, `video_tag`, `uploader_id`, `course_id`, `processed`, `created_at`, `updated_at`)
VALUES
	(1,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL);

/*!40000 ALTER TABLE `course_videos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table courses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `courses`;

CREATE TABLE `courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `instructor_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `instruction_level_id` int(10) unsigned NOT NULL,
  `course_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci,
  `overview` text COLLATE utf8mb4_unicode_ci,
  `course_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course_video` int(10) unsigned DEFAULT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  `strike_out_price` decimal(8,2) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;

INSERT INTO `courses` (`id`, `instructor_id`, `category_id`, `instruction_level_id`, `course_title`, `course_slug`, `keywords`, `overview`, `course_image`, `thumb_image`, `course_video`, `duration`, `price`, `strike_out_price`, `is_active`, `created_at`, `updated_at`)
VALUES
	(1,1,1,1,'Deposit Insurance Training for Bankers','deposit-insurance-training-for-bankers','Financial literacy,KDIC Bankers Campaign','<p>Welcome to this training program that has been put together to help you better understand deposit insurance and how it works in Kenya. This course will help you understand the financial sector, the roles of banks, how client deposits are protected, and your individual role in protecting client deposits.</p>\r\n<p><br />This will enable you to effectively assist your customers understand that their money is protected when they deposit with banks. This will increase their confidence in banks and lead to deposit growth for banks, in turn leading to increased loanable funds, and interest income, thereby contributing to bank and economic growth, which aligns with the Economic Pillar of Vision 2030, were the focus for Financial Services is to create a vibrant and globally competitive financial sector in Kenya that will create jobs and promote high levels of savings to finance Kenya&rsquo;s overall investment needs. As part of Kenya&rsquo;s macro-economic goals, savings rates are targeted to rise from 17-30% of GDP by 2030. <br /> <br />This will be driven through measures such as increasing bank deposits from 44-80% of GDP and by reducing the cost of borrowed capital, that is, interest rates. After going through the course I trust that you will use the knowledge you gain to educate your colleagues, &nbsp;clients and the general public on deposit insurance, and that you frequently review the content and have further discussions with you colleagues on the same &nbsp;to keep it fresh and relevant in your mind. Welcome!</p>\r\n<p>By the end of this course you will be able to:-  </p>\r\n<p>- Define deposit insurance and explain how it works.</p>\r\n<p> - List the main deposit insurance players in Kenya.  </p>\r\n<p>- Outline the roles and responsibilities of banks and KDIC in protecting depositors&rsquo; funds.  </p>\r\n<p>- Outline the process of liquidating a failed bank. </p>\r\n<p>- Explain your personal role in protecting depositors&rsquo; funds.</p>','course/1/KDIC Bankers Campaign(1).jpg','course/1/thumb_KDIC Bankers Campaign(1).jpg',NULL,'2 days',NULL,NULL,1,'2020-01-08 10:18:09','2020-01-08 12:41:24');

/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table credits
# ------------------------------------------------------------

DROP TABLE IF EXISTS `credits`;

CREATE TABLE `credits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` int(10) unsigned NOT NULL,
  `instructor_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `credit` decimal(10,2) DEFAULT NULL,
  `credits_for` int(11) DEFAULT NULL COMMENT '1-course_cost,2-course_commission',
  `is_admin` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `credits` WRITE;
/*!40000 ALTER TABLE `credits` DISABLE KEYS */;

INSERT INTO `credits` (`id`, `transaction_id`, `instructor_id`, `user_id`, `course_id`, `credit`, `credits_for`, `is_admin`, `created_at`, `updated_at`)
VALUES
	(1,1,0,1,1,0.00,2,1,'2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(2,2,1,2,1,0.00,1,0,'2020-01-08 10:20:40','2020-01-08 10:20:40'),
	(3,2,0,2,1,0.00,2,1,'2020-01-08 10:20:40','2020-01-08 10:20:40'),
	(4,3,1,3,1,0.00,1,0,'2020-01-08 11:55:34','2020-01-08 11:55:34'),
	(5,3,0,3,1,0.00,2,1,'2020-01-08 11:55:34','2020-01-08 11:55:34'),
	(6,4,1,4,1,0.00,1,0,'2020-01-08 17:58:32','2020-01-08 17:58:32'),
	(7,4,0,4,1,0.00,2,1,'2020-01-08 17:58:32','2020-01-08 17:58:32'),
	(8,5,1,5,1,0.00,1,0,'2020-01-17 07:02:21','2020-01-17 07:02:21'),
	(9,5,0,5,1,0.00,2,1,'2020-01-17 07:02:21','2020-01-17 07:02:21'),
	(10,6,1,6,1,0.00,1,0,'2020-01-17 07:02:29','2020-01-17 07:02:29'),
	(11,6,0,6,1,0.00,2,1,'2020-01-17 07:02:29','2020-01-17 07:02:29'),
	(12,7,1,7,1,0.00,1,0,'2020-01-17 13:15:22','2020-01-17 13:15:22'),
	(13,7,0,7,1,0.00,2,1,'2020-01-17 13:15:22','2020-01-17 13:15:22'),
	(14,8,1,8,1,0.00,1,0,'2020-01-20 04:22:53','2020-01-20 04:22:53'),
	(15,8,0,8,1,0.00,2,1,'2020-01-20 04:22:54','2020-01-20 04:22:54'),
	(16,9,1,9,1,0.00,1,0,'2020-02-24 15:10:29','2020-02-24 15:10:29'),
	(17,9,0,9,1,0.00,2,1,'2020-02-24 15:10:29','2020-02-24 15:10:29'),
	(18,10,1,10,1,0.00,1,0,'2020-03-20 10:04:23','2020-03-20 10:04:23'),
	(19,10,0,10,1,0.00,2,1,'2020-03-20 10:04:23','2020-03-20 10:04:23'),
	(20,11,1,11,1,0.00,1,0,'2020-03-20 10:11:36','2020-03-20 10:11:36'),
	(21,11,0,11,1,0.00,2,1,'2020-03-20 10:11:36','2020-03-20 10:11:36'),
	(22,12,1,12,1,0.00,1,0,'2020-03-24 12:35:00','2020-03-24 12:35:00'),
	(23,12,0,12,1,0.00,2,1,'2020-03-24 12:35:00','2020-03-24 12:35:00'),
	(24,13,1,13,1,0.00,1,0,'2020-05-12 10:42:45','2020-05-12 10:42:45'),
	(25,13,0,13,1,0.00,2,1,'2020-05-12 10:42:45','2020-05-12 10:42:45');

/*!40000 ALTER TABLE `credits` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table curriculum_lectures_quiz
# ------------------------------------------------------------

DROP TABLE IF EXISTS `curriculum_lectures_quiz`;

CREATE TABLE `curriculum_lectures_quiz` (
  `lecture_quiz_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `section_id` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `contenttext` text COLLATE utf8mb4_unicode_ci,
  `media` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '0-video,1-audio,2-document,3-text',
  `sort_order` int(11) DEFAULT NULL,
  `publish` int(11) NOT NULL DEFAULT '0',
  `resources` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`lecture_quiz_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `curriculum_lectures_quiz` WRITE;
/*!40000 ALTER TABLE `curriculum_lectures_quiz` DISABLE KEYS */;

INSERT INTO `curriculum_lectures_quiz` (`lecture_quiz_id`, `section_id`, `type`, `title`, `description`, `contenttext`, `media`, `media_type`, `sort_order`, `publish`, `resources`, `created_at`, `updated_at`)
VALUES
	(1,1,NULL,'Text Lesson','Est aut mollitia explicabo voluptatem rerum cupiditate molestiae quis est harum qui saepe corrupti facere voluptas distinctio ea placeat eligendi soluta quam eum dolor enim quia ad hic eligendi eos voluptas repudiandae quaerat necessitatibus omnis provident deserunt omnis beatae cum optio sint voluptatum et et voluptatem consequatur quia id.','Nesciunt non quis repellendus ducimus ea laboriosam. Eius voluptatem consectetur harum exercitationem quo ex. Quis dolorum aut aut dolores voluptatibus laudantium voluptas. Impedit qui reprehenderit ut nihil commodi earum fugiat. Voluptatum sunt a cum et odio quisquam eveniet. Architecto tenetur blanditiis corporis corrupti. Ut eveniet minima est veritatis. Ut tenetur voluptatem eos voluptate dolorum eius. Non sed dolore ut quasi cumque libero a. Nemo quod alias dolore sed aspernatur et atque. Rerum iusto nihil et provident aut odio. Aut accusantium asperiores nihil sed ut. Maiores exercitationem quisquam laborum temporibus quaerat inventore autem reprehenderit. Natus corporis qui ipsam minus. Consectetur est enim quo ut. Corporis quae nisi sed iste ut dolorem et facilis. Cumque harum alias quae tempore porro molestiae maiores. Vel exercitationem dolores quod. Et non in quia aut maiores asperiores delectus. Ut qui similique error nisi. Illo et et in officiis dolore. Unde similique illum sed cum ex. Quos odit et quo quasi. Quo nemo ipsa est itaque perferendis. Ex eos ut ut. Repudiandae unde vel soluta beatae. Tempora culpa in est possimus in deleniti repellendus.',NULL,'TEXT',NULL,0,NULL,'2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(2,1,NULL,'Video Lesson','','','1','VIDEO',NULL,1,'[1]','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(3,1,NULL,'Audio Lesson','','','2','AUDIO',NULL,1,'[2]','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(4,1,NULL,'Picture Lesson','','','3','PICTURE',NULL,1,'[3]','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(5,1,NULL,'PDF Lesson','','','4','PDF',NULL,1,'[4]','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(6,2,NULL,'Text Lesson','Et nulla cum quas nam qui est occaecati aut autem saepe velit laborum et reiciendis ducimus dolor dolorum aliquid autem doloribus velit exercitationem sit eaque recusandae quia.','Facere aut occaecati ut molestias. Omnis enim debitis id numquam assumenda. Iste incidunt sit ex laudantium quia aspernatur amet qui. Nemo alias sint quisquam molestiae beatae sint dignissimos. Hic autem fugiat non dolorum est ut. Repellendus mollitia veritatis dicta autem aut quod corporis. Veniam eius enim sit quia. Velit est occaecati et quaerat minus accusamus. Est et quia cumque libero reiciendis non accusamus. Accusamus aut nihil est. Praesentium et nulla labore autem perferendis. Minima perspiciatis sunt ex aut. Veniam pariatur ex et fugit. Asperiores sapiente impedit et ut. Et repellendus expedita qui in porro et. Omnis aut quam exercitationem quaerat. Itaque sunt a necessitatibus incidunt ut eius. Quas eos veritatis impedit saepe aperiam. Voluptates quia debitis sit tempora quas debitis. Qui in unde vitae esse porro laudantium. Odio et doloremque error dolores ut.',NULL,'TEXT',1,1,NULL,'2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(7,2,NULL,'Video Lesson','','','1','VIDEO',2,1,'[1]','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(8,2,NULL,'Audio Lesson','','','2','AUDIO',3,1,'[2]','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(9,2,NULL,'Picture Lesson','','','3','PICTURE',4,1,'[3]','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(10,2,NULL,'PDF Lesson','','','4','PDF',5,1,'[4]','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(11,3,NULL,'Text Lesson','Iusto rerum labore accusamus error eius enim quos qui accusamus cumque ut quam voluptas provident dolorum consequatur laudantium quaerat vero tenetur architecto quaerat corporis.','Numquam recusandae dolorem laborum qui quos. Nam enim aut adipisci dolor unde. Sunt commodi quia autem id. Est labore consequatur explicabo ea error exercitationem. Vel accusantium nam dolor perspiciatis. Rerum dolor sit quis commodi. Qui amet eos ea et. Repellendus eum molestiae autem cupiditate. Nostrum voluptatem aut perspiciatis. Soluta quas quia molestiae. Aliquam voluptate quia iusto quo quo accusamus occaecati quia. Minima sit nostrum et quasi voluptas delectus ipsum. Et magnam assumenda ullam voluptatem dicta iste. Vero fuga sed dicta ut iste. Recusandae quibusdam et non molestiae. Cupiditate quas natus ipsam earum odio ex soluta ut. Maxime rerum alias cupiditate. Reiciendis omnis sed voluptatibus culpa inventore. Quia possimus doloribus ratione sapiente qui deleniti suscipit. Laboriosam voluptate quae facilis minima. Exercitationem harum delectus quo. Minus ex sapiente repudiandae reprehenderit vitae est nesciunt. Hic repudiandae ullam quibusdam. Ut quis et quae. Ut expedita dolores ea fuga quos voluptas. Rerum ducimus non dolorum error voluptates. Inventore velit voluptates est sequi dicta fugit. Cumque repellat dolor sapiente voluptatum nemo. Dolor fugiat repellat dicta quia laudantium voluptatum. Et qui magnam ut a accusamus excepturi. Recusandae ut quia rerum et id animi. Modi non omnis nesciunt dolor dolores tenetur suscipit vel. Id aut numquam nemo tempora est rem. Consectetur reprehenderit porro non ut. Alias error eos fugit aut at cupiditate. Qui aut debitis aut voluptates quia quas. Numquam magni et sapiente nam laboriosam cum est. Accusantium aliquid et nihil consequatur saepe vero veritatis. Dignissimos quo reiciendis officiis. Quae nisi et dolores tempore esse sunt. Quas laborum voluptates in. Et qui dolorum dolor quidem atque sed minima. Temporibus aperiam beatae omnis consectetur. Et nemo voluptatibus porro amet voluptatem. Eos omnis eaque quia porro unde. Minus dolore culpa quos esse minima eos tempore. Rerum laboriosam ut mollitia ut nihil voluptatem. Ut et qui labore soluta perspiciatis perspiciatis accusantium molestiae. Excepturi in dolor in porro dolore. Doloribus autem a tempore qui quos quisquam aperiam. Incidunt voluptatem accusantium minus sunt expedita ipsa perspiciatis. Nesciunt incidunt et tempora eligendi eveniet aut et.',NULL,'TEXT',1,1,NULL,'2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(12,3,NULL,'Video Lesson','','','1','VIDEO',2,1,'[1]','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(13,3,NULL,'Audio Lesson','','','2','AUDIO',3,1,'[2]','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(14,3,NULL,'Picture Lesson','','','3','PICTURE',4,1,'[3]','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(15,3,NULL,'PDF Lesson','','','4','PDF',5,1,'[4]','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(16,4,0,'Awareness of Deposit Insurance',NULL,'<p>\n    <img src=\"https://tdm.co.ke/img/Awareness of Deposit Insurance.jpg\" width=\"100%\">\n</p>',NULL,'TEXT',1,1,NULL,'2020-01-08 10:19:15','2020-03-23 16:14:47'),
	(18,4,0,'The Challenge',NULL,'<p>\n<img src=\"https://tdm.co.ke/img/KDIC Challenge.jpg\" width=\"100%\">\n</p>',NULL,'TEXT',2,1,NULL,'2020-01-08 10:43:01','2020-03-23 16:15:29'),
	(19,5,0,'Module Objectives And Module Outline',NULL,'<p><strong>Module Objectives</strong></p>\n<ul class=\"ui-sortable\">\n<li>Define financial intermediation.</li>\n<li>Describe the main services banks offer.</li>\n<li>Outline how banks contribute to the economy.</li>\n<li> Explain the responsibilities of banks.</li>\n</ul>\n\n\n<p><strong>Module Outline</strong></p>\n<ul class=\"ui-sortable\">\n<li>Financial intermediation.</li>\n<li>Main roles of Banks.</li>\n<li>How banks contribute to the economy. </li>\n<li>Responsibilities of banks.</li>\n</ul>',NULL,'TEXT',4,0,NULL,'2020-01-08 11:32:05','2020-01-08 11:32:05'),
	(20,5,0,'Financial Intermediation',NULL,'<p><img src=\"https://tdm.co.ke/img/Financial Intermediation.jpg\" width=\"100%\">\n</p>\n<p>Facilitating financial transactions by serving as go-between for diverse parties.\nEconomic agents with surplus who want to lend money brought together with those with insufficient funds who need to borrow money.\n</p>',NULL,'TEXT',5,0,NULL,'2020-01-08 11:51:03','2020-01-08 11:51:03'),
	(26,5,0,'Main roles of Banks',NULL,'<p><img src=\"https://tdm.co.ke/img/MainRolesofBanks1.jpg\" width=\"100%\"></p>\n<p><img src=\"https://tdm.co.ke/img/MainRolesofBanks2.jpg\" width=\"100%\"></p>\n<p><img src=\"https://tdm.co.ke/img/MainRolesofBanks3.jpg\" width=\"100%\"></p>',NULL,'TEXT',6,0,NULL,'2020-01-08 12:27:47','2020-01-08 12:27:47'),
	(28,4,0,'Course Objectives and Course Outline',NULL,'<p><strong>Course Objectives</strong><br></p><p>By the end of this module you will be able to:-  </p>\n<ul class=\"ui-sortable\">\n  <li><span>&#10003;</span> Define deposit insurance and explain how it works. </li>\n  <li><span>&#10003;</span> List the main deposit insurance players in Kenya. </li>\n  <li><span>&#10003;</span> Outline the roles and responsibilities of banks and KDIC in protecting depositors’ funds. </li>\n  <li><span>&#10003;</span> Outline the process of liquidating a failed bank.</li>\n  <li><span>&#10003;</span> Explain your personal role in protecting depositors’ funds. </li>\n</ul>\n<p><strong>Course Outline</strong></p>\n<ul class=\"ui-sortable\">\n  <li><span>&#10003;</span> Module 1 –Overview of the banking sector in Kenya. </li>\n  <li><span>&#10003;</span> Module 2 –Roles and responsibilities of Banks.</li>\n  <li><span>&#10003;</span> Module 3 –Understanding deposit insurance.</li>\n  <li><span>&#10003;</span> Module 4 –Roles and responsibilities of KDIC.</li>\n  <li><span>&#10003;</span> Module 5 –Liquidation &winding up of a failed bank.</li>  \n  <li><span>&#10003;</span> Module 6 –Your role in deposit protection.</li>\n</ul>',NULL,'TEXT',3,1,NULL,'2020-01-13 12:12:59','2020-03-23 17:13:33'),
	(29,5,0,'How banks contribute to the economy',NULL,'<p><img src=\"https://tdm.co.ke/img/Contribution to the economy.jpg\" width=\"100%\"></p>',NULL,'TEXT',7,1,NULL,'2020-01-13 12:40:32','2020-01-13 12:40:32'),
	(30,5,0,'Responsibility of Banks',NULL,'<p><img src=\"https://tdm.co.ke/img/MainRolesofBanks2.jpg\" width=\"100%\"></p>',NULL,'TEXT',8,1,NULL,'2020-01-13 12:48:16','2020-01-13 12:48:16'),
	(31,5,0,'Useful Resources',NULL,'<p><a href=\"https://www.ics.ke/wp-content/uploads/bsk-pdf-manager/Principles_of_good_corporate_Governance_Private_Sector_-_CS_Gabriel_Kimani_110.pdf\" target=\"blank\">Principles for Corporate Governance in Kenya</a></p>\n<p><a href=\"https://www.centralbank.go.ke/wp-content/uploads/2016/08/PRUDENTIAL-GUIDELINES.pdf\" target=\"blank\">Prudential guidelines for institutions licensed under the Banking Act</a></p>',NULL,'TEXT',9,0,NULL,'2020-01-13 12:54:16','2020-01-13 12:54:16'),
	(35,4,0,'Training Benefits',NULL,'<p><strong>Training Benefits</strong><br></p>\n<p>The training will benefit you in the following ways: </p>\n<ul class=\"ui-sortable\">\n  <li><span>&#10003;</span> Increased knowledge of the sector and deposit insurance.  </li>\n  <li><span>&#10003;</span> Increased public trust/ confidence in banking leading to more deposits (liability growth). </li>\n  <li><span>&#10003;</span> Asset growth as banks are able to lend more as a result of increased deposits.</li>\n  <li><span>&#10003;</span> Faster realization of personal and Bank KPIs.</li>\n  <li><span>&#10003;</span> Growth of banks –and the Kenyan economy.</li>\n</ul>\n\n<p><strong>Tips for Sucess</strong><br></p>\n<ul class=\"ui-sortable\">\n<li><span>&#10003;</span>  Total time is about 2 hours. Complete the training quickly, in 2-3 sessions, within 2-3 days at most.  </li>\n<li><span>&#10003;</span>  Do the modules when you are free from distractions. </li>\n<li><span>&#10003;</span>  Use the review questions to check your understanding of the module content.</li>\n<li><span>&#10003;</span>  Do the assessment test soon after completing the course. </li>\n<li><span>&#10003;</span>  Share your learning with your colleagues. </li>\n<li><span>&#10003;</span>  Discuss how to apply the training in your team. </li>\n</ul>',NULL,'TEXT',4,1,NULL,'2020-03-23 16:26:08','2020-03-23 16:26:35'),
	(37,7,0,'Module objectives and Module Outline',NULL,'<p><strong>Module Objectives</strong><br></p>\n<p>By the end of this module you will be able to: </p>\n<ul class=\"ui-sortable\">\n<li><span>&#10003;</span> Outline how money is the language of business.</li>\n<li><span>&#10003;</span> List the main regulators in the banking system.</li>\n<li><span>&#10003;</span> Briefly describe the roles they each play.</li>\n</ul>\n<p><strong>Module Outline</strong></p>\n<ul class=\"ui-sortable\">\n<li><span>&#10003;</span> Money: The language of business.</li>\n<li><span>&#10003;</span> Key players in the Banking System</li>\n</ul>',NULL,'TEXT',5,1,NULL,'2020-03-23 16:35:20','2020-03-23 17:11:49'),
	(38,7,0,'Money the language of business',NULL,'<p>\n<img src=\"https://tdm.co.ke/img/Money The Language of Business.jpg\" width=\"100%\">\n</p>',NULL,'TEXT',6,1,NULL,'2020-03-23 16:44:40','2020-03-23 16:46:13'),
	(39,7,0,'Key players in the Banking System',NULL,'<p>\n<img src=\"https://tdm.co.ke/img/Key Players in The Banking Sector.jpg\" width=\"100%\">\n</p>',NULL,'TEXT',7,1,NULL,'2020-03-23 16:45:29','2020-03-23 16:45:29'),
	(40,7,0,'Useful resources',NULL,'<p><strong>Useful Resources</strong><br></p>\n<ul class=\"ui-sortable\">\n  <li><a href=\"https://tdm.co.ke/img/KDIC Banks_Training to Banks_Final Pre-Pilot Content.pdf\" target=\"blank\">PDF notes of Module content.</a></li>\n  <li><a href=\"https://kdic.go.ke/index.php/failure-resolution/related-acts-of-parliament\" target=\"blank\">Banking Act (Amendment ), 2016.</a> </li>\n  <li><a href=\"https://kdic.go.ke/index.php/failure-resolution/related-acts-of-parliament\" target=\"blank\">Kenya Deposit Insurance Act, 2012.</a></li>\n</ul>',NULL,'TEXT',8,1,NULL,'2020-03-23 16:51:09','2020-03-23 16:51:19'),
	(42,8,0,'Module objectives and Module Outline',NULL,'<p><strong>Module Objectives</strong></p>\n<ul class=\"ui-sortable\">\n<li><span>&#10003;</span> Define financial intermediation.</li>\n<li><span>&#10003;</span> Describe the main services banks offer.</li>\n<li><span>&#10003;</span> Outline how banks contribute to the economy.</li>\n<li><span>&#10003;</span> Explain the responsibilities of banks.</li>\n</ul>\n\n\n<p><strong>Module Outline</strong></p>\n<ul class=\"ui-sortable\">\n<li><span>&#10003;</span> Financial intermediation.</li>\n<li><span>&#10003;</span> Main roles of Banks.</li>\n<li><span>&#10003;</span> How banks contribute to the economy. </li>\n<li><span>&#10003;</span> Responsibilities of banks.</li>\n</ul>',NULL,'TEXT',9,1,NULL,'2020-03-23 17:17:43','2020-03-23 17:17:43'),
	(43,8,0,'Financial Intermediation',NULL,'<p>\n<img src=\"https://tdm.co.ke/img/Financial Intermediation.jpg\" width=\"100%\">\n</p>\n<p>Facilitating financial transactions by serving as go-between for diverse parties. Economic agents with surplus who want to lend money brought together with those with insufficient funds who need to borrow money.\n</p>',NULL,'TEXT',10,1,NULL,'2020-03-23 17:29:06','2020-03-23 17:29:06'),
	(44,8,0,'Main Roles of Banks',NULL,'<p>\n<img src=\"https://tdm.co.ke/img/Roles of Banks.jpg\" width=\"100%\">\n</p>\n<p>\n<img src=\"https://tdm.co.ke/img/Roles of Banks Deposit Taking.jpg\" width=\"100%\">\n</p>\n<p>\n<img src=\"https://tdm.co.ke/img/Roles of Banks money transmission.jpg\" width=\"100%\">\n</p>',NULL,'TEXT',11,1,NULL,'2020-03-23 18:04:36','2020-03-23 18:04:36'),
	(45,8,0,'How banks contribute to the economy',NULL,'<p>\n<img src=\"https://tdm.co.ke/img/Main roles of banks contribution to the economy.jpg\" width=\"100%\">\n</p>',NULL,'TEXT',12,1,NULL,'2020-03-23 18:07:54','2020-03-23 18:07:54'),
	(46,8,0,'Responsibility of Banks',NULL,'<p>\n<img src=\"https://tdm.co.ke/img/Responsibility of Banks.jpg\" width=\"100%\">\n</p>',NULL,'TEXT',13,1,NULL,'2020-03-23 18:10:55','2020-03-23 18:10:55'),
	(47,8,0,'Useful Resources',NULL,'<p><strong>Useful Resources</strong><br></p>\n<ul class=\"ui-sortable\">\n  <li><a href=\"https://tdm.co.ke/img/KDIC Banks_Training to Banks_Final Pre-Pilot Content.pdf\" target=\"blank\">PDF notes of Module content.</a></li>\n  <li><a href=\"https://www.ics.ke/wp-content/uploads/bsk-pdf-manager/Principles_of_good_corporate_Governance_Private_Sector_-_CS_Gabriel_Kimani_110.pdf\" target=\"blank\">Banking Principles of Good Corporate Governance.</a> </li>\n  <li><a href=\"https://www.centralbank.go.ke/wp-content/uploads/2016/08/PRUDENTIAL-GUIDELINES.pdf\" target=\"blank\">Kenya Central Bank of Kenya Prudential Guidelines.</a></li>\n<ul>',NULL,'TEXT',14,1,NULL,'2020-03-23 18:14:31','2020-03-23 18:14:31'),
	(48,9,0,'Module objectives and Module Outline',NULL,'<p><strong>Module Objectives</strong></p>\n  <ul class=\"ui-sortable\">\n  <li><span>&#10003;</span> Explain what happens when the financial intermediation chain is broken.</li>\n  <li><span>&#10003;</span> Define deposit insurance.</li>\n  <li><span>&#10003;</span> Explain the benefits of deposit insurance.</li>\n  <li><span>&#10003;</span> Outline the history of deposit insurance in Kenya.</li>\n  </ul>\n\n\n  <p><strong>Module Outline</strong></p>\n  <ul class=\"ui-sortable\">\n  <li><span>&#10003;</span> When the financial intermediation chain is broken. </li>\n  <li><span>&#10003;</span> Defining deposit insurance.</li>\n  <li><span>&#10003;</span> Benefits of deposit insurance. </li>\n  <li><span>&#10003;</span> History of deposit insurance in Kenya.</li>\n  </ul>',NULL,'TEXT',15,1,NULL,'2020-04-16 16:42:37','2020-04-16 16:43:24'),
	(49,9,0,'When the financial intermediation chain is broken',NULL,'<p>\n  <img src=\"https://tdm.co.ke/img/Review of Financial Intermediation.jpg\" width=\"100%\">\n  </p>\n  <p>\n  <img src=\"https://tdm.co.ke/img/When the financial intermediation chain is broken.jpg\" width=\"100%\">\n  </p>',NULL,'TEXT',16,1,NULL,'2020-04-16 16:44:10','2020-04-16 16:44:10'),
	(50,9,0,'Benefits of deposit insurance',NULL,'<p>\n  <img src=\"https://tdm.co.ke/img/Benefits of deposit insurance.jpg\" width=\"100%\">\n  </p>',NULL,'TEXT',18,1,NULL,'2020-04-16 16:44:26','2020-04-16 16:48:16'),
	(51,9,0,'Defining deposit insurance',NULL,'<p>\n  <img src=\"https://tdm.co.ke/img/Defining deposit insurance.jpg\" width=\"100%\">\n  </p>',NULL,'TEXT',17,1,NULL,'2020-04-16 16:45:56','2020-04-16 16:46:34'),
	(52,9,0,'History of deposit insurance in Kenya',NULL,'<p>\n  <img src=\"https://tdm.co.ke/img/History of deposit insurance in Kenya.jpg\" width=\"100%\">\n  </p>\n  <p>\n  <img src=\"https://tdm.co.ke/img/History of deposit insurance in Kenya2.jpg\" width=\"100%\">\n  </p>',NULL,'TEXT',19,1,NULL,'2020-04-16 16:46:45','2020-04-16 16:48:49'),
	(53,9,0,'Useful resources',NULL,'<p><strong>Useful Resources</strong><br></p>\n  <ul class=\"ui-sortable\">\n    <li><a href=\"https://tdm.co.ke/img/KDIC Banks_Training to Banks_Final Pre-Pilot Content.pdf\" target=\"blank\">PDF notes of Module content.</a></li>\n    <li><a href=\"https://en.wikipedia.org/wiki/Consolidated_Bank_of_Kenya\" target=\"blank\">List of 9 banks that formed consolidated bank: </a> </li>\n    <li><a href=\"https://kdic.go.ke/index.php/failure-resolution/related-acts-of-parliament\" target=\"blank\">Kenya Deposit Insurance Act, 2012.</a></li>\n      <li><a href=\"http://www.kdic.go.ke/index.php/members-institutions\" target=\"blank\">KDIC list of member institutions.</a></li>\n  </ul>',NULL,'TEXT',20,1,NULL,'2020-04-16 16:52:34','2020-04-16 16:52:34'),
	(54,10,0,'Module objectives and Module Outline',NULL,'<p><strong>Module Objectives</strong></p>\n  <ul class=\"ui-sortable\">\n  <li><span>&#10003;</span> Outline KDIC’s mission, vision and values. </li>\n  <li><span>&#10003;</span> Explain how to become a member of KDIC. </li>\n  <li><span>&#10003;</span> Outline KDIC’s roles.</li>\n  <li><span>&#10003;</span> Describe KDIC’s business as usual responsibilities. </li>\n    <li><span>&#10003;</span> Describe the process of receivership for a bank. </li>\n  </ul>\n\n\n  <p><strong>Module Outline</strong></p>\n  <ul class=\"ui-sortable\">\n  <li><span>&#10003;</span> KDIC’s mission, vision and values. </li>\n  <li><span>&#10003;</span> Membership in KDIC. </li>\n  <li><span>&#10003;</span> KDIC’s roles. </li>\n  <li><span>&#10003;</span> KDIC’s business as usual responsibilities. </li>\n    <li><span>&#10003;</span> Receivership process for a bank.  </li>\n  </ul>',NULL,'TEXT',1,1,NULL,'2020-04-16 17:18:27','2020-04-16 17:19:04'),
	(55,10,0,'KDIC’s mission, vision and values',NULL,'<p>\n  <img src=\"https://tdm.co.ke/img/KDIC’s mission, vision and values.jpg\" width=\"100%\">\n  </p>',NULL,'TEXT',22,1,NULL,'2020-04-16 17:19:56','2020-04-16 17:19:56'),
	(56,10,0,'Membership in KDIC',NULL,'<p>\n  <img src=\"https://tdm.co.ke/img/Membership in KDIC.jpg\" width=\"100%\">\n  </p>',NULL,'TEXT',23,1,NULL,'2020-04-16 17:20:39','2020-04-16 17:20:39'),
	(57,10,0,'KDIC’s roles',NULL,'<p>\n  <img src=\"https://tdm.co.ke/img/KDIC’s roles.jpg\" width=\"100%\">\n  </p>',NULL,'TEXT',24,1,NULL,'2020-04-16 17:21:15','2020-04-16 17:21:15'),
	(58,10,0,'KDIC’s business as usual responsibilities',NULL,'<p>\n  <img src=\"https://tdm.co.ke/img/KDIC’s business as usual responsibilities.jpg\" width=\"100%\">\n  </p>',NULL,'TEXT',25,1,NULL,'2020-04-16 17:21:48','2020-04-16 17:21:48'),
	(59,10,0,'Receivership process for a bank',NULL,'<p>\n  <img src=\"https://tdm.co.ke/img/Receivership process for a bank.jpg\" width=\"100%\">\n  </p>\n\n  <p>\n  <img src=\"https://tdm.co.ke/img/Receivership process for a bank2.jpg\" width=\"100%\">\n  </p>',NULL,'TEXT',26,1,NULL,'2020-04-16 17:22:22','2020-04-16 17:22:22'),
	(60,10,0,'Useful resources',NULL,'<p><strong>Useful Resources</strong><br></p>\n  <ul class=\"ui-sortable\">\n    <li><a href=\"https://tdm.co.ke/img/KDIC Banks_Training to Banks_Final Pre-Pilot Content.pdf\" target=\"blank\">PDF notes of Module content.</a></li>\n    <li><a href=\"https://kdic.go.ke/index.php/failure-resolution/related-acts-of-parliament\" target=\"blank\">Kenya Deposit Insurance Act, 2012.</a></li>\n      <li><a href=\"http://www.kdic.go.ke/index.php/resource-centre/resolution\" target=\"blank\">KDIC intervention mechanisms. </a></li>\n  </ul>',NULL,'TEXT',27,1,NULL,'2020-04-16 17:23:01','2020-04-16 17:23:01'),
	(61,11,0,'Module objectives and Module Outline',NULL,'<p><strong>Module Objectives</strong></p>\n  <ul class=\"ui-sortable\">\n  <li><span>&#10003;</span> Define liquidation and winding up.  </li>\n  <li><span>&#10003;</span> Outline the process for liquidating and winding up a bank.  </li>\n  <li><span>&#10003;</span> Explain how protected deposits are paid. </li>\n  <li><span>&#10003;</span> List the accounts covered and those not covered as protected deposits.  </li>\n  </ul>\n\n\n  <p><strong>Module Outline</strong></p>\n  <ul class=\"ui-sortable\">\n  <li><span>&#10003;</span> Review: KDIC’s roles.  </li>\n  <li><span>&#10003;</span>  Definitions: Liquidation and winding up.  </li>\n  <li><span>&#10003;</span> Bank liquidation and winding up process.  </li>\n  <li><span>&#10003;</span> Payment of protected deposits. </li>\n    <li><span>&#10003;</span> Accounts covered/ not covered.  </li>\n  </ul>',NULL,'TEXT',1,1,NULL,'2020-04-16 17:43:30','2020-04-16 17:43:48'),
	(62,11,0,'Review: KDIC’s roles',NULL,'<p>\n  <img src=\"https://tdm.co.ke/img/Review- KDIC’s roles.jpg\" width=\"100%\">\n  </p>',NULL,'TEXT',29,1,NULL,'2020-04-16 17:44:28','2020-04-16 17:44:28'),
	(63,11,0,'Definitions: Liquidation and winding up',NULL,'<p>\n  <img src=\"https://tdm.co.ke/img/Definitions- Liquidation and winding up.jpg\" width=\"100%\">\n  </p>',NULL,'TEXT',30,1,NULL,'2020-04-16 17:45:05','2020-04-16 17:45:05'),
	(64,11,0,'Bank liquidation and winding up process',NULL,'<p>\n  <img src=\"https://tdm.co.ke/img/Bank liquidation and winding up process.jpg\" width=\"100%\">\n  </p>',NULL,'TEXT',31,1,NULL,'2020-04-16 17:45:36','2020-04-16 17:45:36'),
	(65,11,0,'Payment of protected deposits',NULL,'<p>\n  <img src=\"https://tdm.co.ke/img/Payment of protected deposits.jpg\" width=\"100%\">\n  </p>',NULL,'TEXT',32,1,NULL,'2020-04-16 17:46:05','2020-04-16 17:46:05'),
	(66,11,0,'Accounts covered/ not covered',NULL,'<p>\n  <img src=\"https://tdm.co.ke/img/Accounts covered not covered.jpg\" width=\"100%\">\n  </p>',NULL,'TEXT',33,1,NULL,'2020-04-16 17:46:34','2020-04-16 17:46:34'),
	(67,11,0,'Useful resources',NULL,'<p><strong>Useful Resources</strong><br></p>\n  <ul class=\"ui-sortable\">\n    <li><a href=\"https://tdm.co.ke/img/KDIC Banks_Training to Banks_Final Pre-Pilot Content.pdf\" target=\"blank\">PDF notes of Module content.</a></li>\n    <li><a href=\"http://www.kdic.go.ke/images/docs/LIST_OF_INSTITUTIONS_PLACED_IN_LIQUIDATION.pdf\" target=\"blank\">List of banks placed in liquidation.</a></li>\n      <li><a href=\"http://www.kdic.go.ke/index.php/resource-centre/resolution\" target=\"blank\">List of wound up banks. </a></li>\n  </ul>',NULL,'TEXT',34,1,NULL,'2020-04-16 17:47:03','2020-04-16 17:47:03'),
	(68,12,0,'Module objectives and Module Outline',NULL,'<p><strong>Module Objectives</strong></p>\n  <ul class=\"ui-sortable\">\n  <li><span>&#10003;</span> Explain how to carry out your moral responsibility in deposit protection.  </li>\n  <li><span>&#10003;</span> Describe personal, bank and national benefits of you playing your role and the impact of not playing your role.  </li>\n  <li><span>&#10003;</span> Outline how to blow the whistle if you notice anything suspicious and consequences of not blowing the whistle. </li>\n  </ul>\n\n\n  <p><strong>Module Outline</strong></p>\n  <ul class=\"ui-sortable\">\n  <li><span>&#10003;</span> Your moral responsibility. </li>\n  <li><span>&#10003;</span>  Benefits of playing your role.  </li>\n  <li><span>&#10003;</span> Impact of not playing your role.  </li>\n  <li><span>&#10003;</span> Whistle-blowing guidelines.  </li>\n    <li><span>&#10003;</span> Consequences of not blowing the whistle.  </li>\n  </ul>',NULL,'TEXT',1,1,NULL,'2020-04-16 18:14:03','2020-04-16 18:14:20'),
	(69,12,0,'Your moral responsibility',NULL,'<p>\n  <img src=\"https://tdm.co.ke/img/Your moral responsibility.jpg\" width=\"100%\">\n  </p>\n  <p>\n  <img src=\"https://tdm.co.ke/img/Your moral responsibility2.jpg\" width=\"100%\">\n  </p>',NULL,'TEXT',36,1,NULL,'2020-04-16 18:14:44','2020-04-16 18:14:44'),
	(70,12,0,'Benefits of playing your role',NULL,'<p>\n  <img src=\"https://tdm.co.ke/img/Benefits of playing your role.jpg\" width=\"100%\">\n  </p>',NULL,'TEXT',37,1,NULL,'2020-04-16 18:15:18','2020-04-16 18:15:18'),
	(71,12,0,'Impact of not playing your role',NULL,'<p>\n  <img src=\"https://tdm.co.ke/img/Impact of not playing your role.jpg\" width=\"100%\">\n  </p>',NULL,'TEXT',38,1,NULL,'2020-04-16 18:15:42','2020-04-16 18:15:42'),
	(72,12,0,'Whistle-blowing guidelines',NULL,'<p>\n  <img src=\"https://tdm.co.ke/img/Whistle-blowing guidelines.jpg\" width=\"100%\">\n  </p>\n  <p>\n  <img src=\"https://tdm.co.ke/img/Whistle-blowing guidelines2.jpg \"width=\"100%\">\n  </p>',NULL,'TEXT',39,1,NULL,'2020-04-16 18:16:06','2020-04-16 18:16:06'),
	(73,12,0,'Consequences of not blowing the whistle',NULL,'<p>\n  <img src=\"https://tdm.co.ke/img/Consequences of not blowing the whistle.jpg\" width=\"100%\">\n  </p>',NULL,'TEXT',40,1,NULL,'2020-04-16 18:18:20','2020-04-16 18:18:20'),
	(74,12,0,'Useful resources',NULL,'<p><strong>Useful Resources</strong><br></p>\n  <ul class=\"ui-sortable\">\n    <li><a href=\"https://tdm.co.ke/img/KDIC Banks_Training to Banks_Final Pre-Pilot Content.pdf\" target=\"blank\">PDF notes of Module content.</a></li>\n    <li><a href=\"https://www2.deloitte.com/na/en/pages/risk/articles/why-blow-the-whistle-toa.html\" target=\"blank\">Online article: Why blow the whistle.</a></li>\n    \n  </ul>',NULL,'TEXT',41,1,NULL,'2020-04-16 18:18:53','2020-04-16 18:18:53');

/*!40000 ALTER TABLE `curriculum_lectures_quiz` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table curriculum_sections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `curriculum_sections`;

CREATE TABLE `curriculum_sections` (
  `section_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) DEFAULT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `exam_id` int(11) DEFAULT NULL,
  `createdOn` datetime NOT NULL,
  `updatedOn` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `curriculum_sections` WRITE;
/*!40000 ALTER TABLE `curriculum_sections` DISABLE KEYS */;

INSERT INTO `curriculum_sections` (`section_id`, `course_id`, `title`, `sort_order`, `exam_id`, `createdOn`, `updatedOn`, `created_at`, `updated_at`)
VALUES
	(4,1,'Introduction To The Training',1,NULL,'2020-03-23 16:14:42','2020-03-23 16:14:42','2020-01-08 10:18:53','2020-01-08 10:18:53'),
	(7,1,'Module 1: Overview of the Banking Sector',2,4,'2020-03-23 16:34:17','2020-03-23 16:34:17','2020-03-23 16:34:17','2020-03-23 17:10:23'),
	(8,1,'Module 2: Roles and Responsibilities of Banks',3,NULL,'2020-03-23 17:17:15','2020-03-23 17:17:15','2020-03-23 17:17:15','2020-03-23 17:17:15'),
	(9,1,'Module 3: Overview of Deposit Insurance',4,NULL,'2020-04-16 16:42:30','2020-04-16 16:42:30','2020-04-16 16:42:30','2020-04-16 16:42:30'),
	(10,1,'Module 4: Roles and Responsibilities of KDIC',5,NULL,'2020-04-16 17:18:52','2020-04-16 17:18:52','2020-04-16 17:18:22','2020-04-16 17:18:22'),
	(11,1,'Module 5: Liquidation of a Failed Bank',6,NULL,'2020-04-16 17:43:25','2020-04-16 17:43:25','2020-04-16 17:43:25','2020-04-16 17:43:25'),
	(12,1,'Module 6: Your Role in Deposit Protection',7,NULL,'2020-04-16 18:13:59','2020-04-16 18:13:59','2020-04-16 18:13:59','2020-04-16 18:13:59');

/*!40000 ALTER TABLE `curriculum_sections` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exams`;

CREATE TABLE `exams` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `section_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `exams` WRITE;
/*!40000 ALTER TABLE `exams` DISABLE KEYS */;

INSERT INTO `exams` (`id`, `section_id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,5,'KDIC – Module 2 Questions and answers','2020-01-08 10:18:09','2020-01-09 08:31:13'),
	(2,2,'Occaecati reiciendis accusantium.','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(3,3,'Recusandae consequuntur sunt.','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(4,7,'KDIC – Module 1 Questions and answers','2020-03-23 16:55:55','2020-03-23 17:10:23');

/*!40000 ALTER TABLE `exams` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table instruction_levels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `instruction_levels`;

CREATE TABLE `instruction_levels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `level` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `instruction_levels` WRITE;
/*!40000 ALTER TABLE `instruction_levels` DISABLE KEYS */;

INSERT INTO `instruction_levels` (`id`, `level`)
VALUES
	(1,'Simple Reading'),
	(2,'Intermediate Reading'),
	(3,'Advanced Reading');

/*!40000 ALTER TABLE `instruction_levels` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table instructors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `instructors`;

CREATE TABLE `instructors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instructor_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_googleplus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biography` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `instructor_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_credits` decimal(10,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `instructors` WRITE;
/*!40000 ALTER TABLE `instructors` DISABLE KEYS */;

INSERT INTO `instructors` (`id`, `user_id`, `first_name`, `last_name`, `instructor_slug`, `contact_email`, `telephone`, `mobile`, `paypal_id`, `link_facebook`, `link_linkedin`, `link_twitter`, `link_googleplus`, `biography`, `instructor_image`, `total_credits`, `created_at`, `updated_at`)
VALUES
	(1,3,'Instructor','Account','instructor-account','instructor@gmail.com','0700 123 456','0700 123 456','instructor@gmail.com',NULL,NULL,NULL,NULL,'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>','frontend/img/avatar.jpg',0.00,'2020-01-08 10:18:09','2020-05-12 10:42:45');

/*!40000 ALTER TABLE `instructors` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(257,'2019_02_18_062028_create_categories_table',1),
	(258,'2019_02_18_062028_create_course_files_table',1),
	(259,'2019_02_18_062028_create_course_videos_table',1),
	(260,'2019_02_18_062028_create_courses_table',1),
	(261,'2019_02_18_062028_create_curriculum_lectures_quiz_table',1),
	(262,'2019_02_18_062028_create_curriculum_sections_table',1),
	(263,'2019_02_18_062028_create_instruction_levels_table',1),
	(264,'2019_02_18_062028_create_password_resets_table',1),
	(265,'2019_02_18_062028_create_role_user_table',1),
	(266,'2019_02_18_062028_create_roles_table',1),
	(267,'2019_02_18_062028_create_users_table',1),
	(268,'2019_02_22_063348_create_instructors_table',1),
	(269,'2019_02_22_151526_create_payments_table',1),
	(270,'2019_03_02_084257_create_course_ratings',1),
	(271,'2019_03_03_072224_create_blogs_table',1),
	(272,'2019_03_04_141453_create_options_table',1),
	(273,'2019_03_08_072337_create_withdraw_requests_table',1),
	(274,'2019_04_07_145907_create_course_progress',1),
	(275,'2019_11_22_234305_create_exams_table',1),
	(276,'2019_11_24_111147_create_questions_table',1),
	(277,'2019_12_16_084454_create_scores_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table options
# ------------------------------------------------------------

DROP TABLE IF EXISTS `options`;

CREATE TABLE `options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_value` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `options` WRITE;
/*!40000 ALTER TABLE `options` DISABLE KEYS */;

INSERT INTO `options` (`id`, `code`, `option_key`, `option_value`)
VALUES
	(1,'pageHome','banner_title','Title goes here'),
	(2,'pageHome','banner_text','Slogan goes here'),
	(3,'pageHome','instructor_text','Even more content will be inserted here'),
	(4,'pageHome','learn_block_title','Some text here'),
	(5,'pageHome','learn_block_text','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.'),
	(6,'pageAbout','content','<article class=\"container\">\n            <div class=\"row\">\n                <div class=\"col-12\">\n                   <h5 class=\"mt-3 underline-heading\">OUR MISSION IS SIMPLE</h5>\n                   <p>Cobem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla dolor sit amet, consectetuer adipiscing elit. </p>\n                   <p> Aenean commodo ligula eget dolor. Aenean massa. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, eta rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis. Lorem ipsum dolor sit amet,Aenean commodo ligula eget dolor. Aenean massa. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, eta rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis. Lorem ipsum dolor sit amet,</p>\n\n                   <ul class=\"ul-no-padding about-ul\">\n                        <li>Commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Commodo ligula eget dolor. Aenean massa. Port sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</li>\n                        <li>Dum sociis natoque penatibus et magnis dis parturient montes</li>\n                        <li>Nascetur ridiculus mus, Nulla consequat massa quis enim, Cum sociis natoque penatibus et magnis dis parturient montes</li>\n                        <li>Commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.Commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</li>\n                        <li>Nascetur ridiculus mus, Nulla consequat massa quis enim  </li>\n                        <li>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus, Nulla consequat massa quis enim </li>\n                        <li>Consectetuer adipiscing elit. Aenean commodo ligula eget dolor</li>\n                        \n                    </ul>\n                </div>\n            </div>\n        </article><article class=\"count-block jumbotron\">\n        <div class=\"container\">\n            <div class=\"row\">\n                <div class=\"col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6\">\n                    <h3 class=\"underline-heading\">150</h3>\n                    <h6>COUNTRIES REACHED</h6>\n                </div>\n                <div class=\"col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6\">\n                    <h3 class=\"underline-heading\">850</h3>\n                    <h6>COUNTRIES REACHED</h6>\n                </div>\n                <div class=\"col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6\">\n                    <h3 class=\"underline-heading\">38300</h3>\n                    <h6>PASSED GRADUATES</h6>\n                </div>\n                <div class=\"col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6\">\n                    <h3 class=\"underline-heading\">3400</h3>\n                    <h6>COURSES PUBLISHED</h6>\n                </div>\n            </div>\n        </div>\n    </article><article class=\"about-features-block\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-12 text-center seperator-head mt-3\">\n                <h3>Why choose Ulearn</h3>\n                <p class=\"mt-3\">Cum doctus civibus efficiantur in imperdiet deterruisset.</p>\n            </div>\n        </div>\n                        <div class=\"row mt-4 mb-5\">\n                                <div class=\"col-xl-3 col-lg-4 col-md-6 col-sm-6\">\n                <div class=\"feature-box mx-auto text-center\">\n                    <main>\n                        <i class=\"fas fa-file-signature\"></i>\n                        <div class=\"col-md-12\">\n                            <h6 class=\"instructor-title\">Hi-Tech Learning </h6>\n                            <p>Aenean massa. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. </p>\n                        </div>\n                    </main>\n                </div>\n            </div>\n                            <div class=\"col-xl-3 col-lg-4 col-md-6 col-sm-6\">\n                <div class=\"feature-box mx-auto text-center\">\n                    <main>\n                        <i class=\"fas fa-users-cog\"></i>\n                        <div class=\"col-md-12\">\n                            <h6 class=\"instructor-title\">Course Discussion </h6>\n                            <p>Aenean massa. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. </p>\n                        </div>\n                    </main>\n                </div>\n            </div>\n                            <div class=\"col-xl-3 col-lg-4 col-md-6 col-sm-6\">\n                <div class=\"feature-box mx-auto text-center\">\n                    <main>\n                        <i class=\"fas fa-shield-alt\"></i>\n                        <div class=\"col-md-12\">\n                            <h6 class=\"instructor-title\">Website Security </h6>\n                            <p>Aenean massa. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. </p>\n                        </div>\n                    </main>\n                </div>\n            </div>\n                            <div class=\"col-xl-3 col-lg-4 col-md-6 col-sm-6\">\n                <div class=\"feature-box mx-auto text-center\">\n                    <main>\n                        <i class=\"fas fa-chalkboard-teacher\"></i>\n                        <div class=\"col-md-12\">\n                            <h6 class=\"instructor-title\">Qualified teachers </h6>\n                            <p>Aenean massa. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. </p>\n                        </div>\n                    </main>\n                </div>\n            </div>\n                            <div class=\"col-xl-3 col-lg-4 col-md-6 col-sm-6\">\n                <div class=\"feature-box mx-auto text-center\">\n                    <main>\n                        <i class=\"fas fa-building\"></i>\n                        <div class=\"col-md-12\">\n                            <h6 class=\"instructor-title\">Equiped class rooms </h6>\n                            <p>Aenean massa. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. </p>\n                        </div>\n                    </main>\n                </div>\n            </div>\n                            <div class=\"col-xl-3 col-lg-4 col-md-6 col-sm-6\">\n                <div class=\"feature-box mx-auto text-center\">\n                    <main>\n                        <i class=\"fas fa-digital-tachograph\"></i>\n                        <div class=\"col-md-12\">\n                            <h6 class=\"instructor-title\">Advanced teaching </h6>\n                            <p>Aenean massa. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. </p>\n                        </div>\n                    </main>\n                </div>\n            </div>\n                            <div class=\"col-xl-3 col-lg-4 col-md-6 col-sm-6\">\n                <div class=\"feature-box mx-auto text-center\">\n                    <main>\n                        <i class=\"fas fa-puzzle-piece\"></i>\n                        <div class=\"col-md-12\">\n                            <h6 class=\"instructor-title\">Adavanced study plans </h6>\n                            <p>Aenean massa. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. </p>\n                        </div>\n                    </main>\n                </div>\n            </div>\n                            <div class=\"col-xl-3 col-lg-4 col-md-6 col-sm-6\">\n                <div class=\"feature-box mx-auto text-center\">\n                    <main>\n                        <i class=\"fas fa-bullseye\"></i>\n                        <div class=\"col-md-12\">\n                            <h6 class=\"instructor-title\">Focus on target </h6>\n                            <p>Aenean massa. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. </p>\n                        </div>\n                    </main>\n                </div>\n            </div>\n                            <div class=\"col-xl-3 col-lg-4 col-md-6 col-sm-6\">\n                <div class=\"feature-box mx-auto text-center\">\n                    <main>\n                        <i class=\"fas fa-thumbs-up\"></i>\n                        <div class=\"col-md-12\">\n                            <h6 class=\"instructor-title\">Focus on success </h6>\n                            <p>Aenean massa. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. </p>\n                        </div>\n                    </main>\n                </div>\n            </div>\n                            <div class=\"col-xl-3 col-lg-4 col-md-6 col-sm-6\">\n                <div class=\"feature-box mx-auto text-center\">\n                    <main>\n                        <i class=\"fas fa-tablet-alt\"></i>\n                        <div class=\"col-md-12\">\n                            <h6 class=\"instructor-title\">Responsive Design </h6>\n                            <p>Aenean massa. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. </p>\n                        </div>\n                    </main>\n                </div>\n            </div>\n                            <div class=\"col-xl-3 col-lg-4 col-md-6 col-sm-6\">\n                <div class=\"feature-box mx-auto text-center\">\n                    <main>\n                        <i class=\"fas fa-credit-card\"></i>\n                        <div class=\"col-md-12\">\n                            <h6 class=\"instructor-title\">Payment Gateways </h6>\n                            <p>Aenean massa. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. </p>\n                        </div>\n                    </main>\n                </div>\n            </div>\n                            <div class=\"col-xl-3 col-lg-4 col-md-6 col-sm-6\">\n                <div class=\"feature-box mx-auto text-center\">\n                    <main>\n                        <i class=\"fas fa-search-plus\"></i>\n                        <div class=\"col-md-12\">\n                            <h6 class=\"instructor-title\">SEO Friendly </h6>\n                            <p>Aenean massa. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. </p>\n                        </div>\n                    </main>\n                </div>\n            </div>\n                        </div>\n    </div>\n</article>'),
	(7,'pageContact','telephone','0700 123 456'),
	(8,'pageContact','email','johndoe@example.com'),
	(9,'pageContact','address','14th Floor, Building Name, Street'),
	(10,'pageContact','map','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3022.940622898076!2d-74.00543578509465!3d40.74133204375838!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c259bf14f1f51f%3A0xcc1b5ab35bd75df0!2sGoogle!5e0!3m2!1sen!2sin!4v1542693598934\" width=\"100%\" height=\"100%\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>'),
	(11,'settingGeneral','application_name','KDIC LMS'),
	(12,'settingGeneral','meta_key','KDIC Learning Management Portal'),
	(13,'settingGeneral','meta_description','KDIC Learning Management Portal'),
	(14,'settingGeneral','admin_commission','0'),
	(15,'settingGeneral','admin_email','admin@example.com'),
	(16,'settingGeneral','minimum_withdraw','100'),
	(17,'settingGeneral','header_logo','config/logo.png'),
	(18,'settingGeneral','fav_icon','config/favicon.ico'),
	(19,'settingGeneral','footer_logo','config/logo_footer.png'),
	(20,'settingPayment','username',''),
	(21,'settingPayment','password',''),
	(22,'settingPayment','signature',''),
	(23,'settingPayment','test_mode','1'),
	(24,'settingPayment','is_active','1'),
	(25,'settingEmail','smtp_host',NULL),
	(26,'settingEmail','smtp_port',NULL),
	(27,'settingEmail','smtp_secure',NULL),
	(28,'settingEmail','smtp_username',NULL),
	(29,'settingEmail','smtp_password',NULL);

/*!40000 ALTER TABLE `options` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;

INSERT INTO `password_resets` (`email`, `token`, `created_at`)
VALUES
	('cmwazi@gmail.com','$2y$10$UXEdw2ueo1DrXPgXszboTutePAH6heyHael7zeEUxW6L4Qrsj2NJy','2020-05-11 16:16:54');

/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table questions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `questions`;

CREATE TABLE `questions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `score` int(11) NOT NULL,
  `choices` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `reasons` text COLLATE utf8mb4_unicode_ci,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;

INSERT INTO `questions` (`id`, `exam_id`, `question`, `score`, `choices`, `reasons`, `answer`, `created_at`, `updated_at`)
VALUES
	(1,1,'Question 1–What is financial intermediation?',2,'[\"Providing finances to borrowers and making sure they pay back.\",\"Paying money out to depositors.\",\"Moving money around an economy.\",\"Facilitating financial transactions by acting as a go-between between depositors and lend to banks and borrowers.\"]','[\"Incorrect.This is part of financial intermediation, but is not the full picture.\",\"Incorrect.This is part of financial intermediation, but is not the full picture.\",\"Incorrect. This is only a part of financial intermediation, but is not the full picture.\",\"Correct. Depositors and borrowers do not know one another. Banks act as go-betweenfor them by receiving money from depositors and lending the same to borrowers.\"]','Facilitating financial transactions by acting as a go-between between depositors and lend to banks and borrowers.','2020-01-08 10:18:09','2020-03-23 18:19:45'),
	(2,1,'Question 2–What are the main categories of services offered by banks?',2,'[\"Deposit-taking, lending, money-transmission.\",\"Customer service, lending, training staff.\",\"Offering loans, bancassurance, telephone banking.\",\"Trade finance, foreign exchange, savings accounts.\"]','[\"Correct. These are the main categories of services offered by banks.\",\"Incorrect. While banks carry outtheseactivities, these are not the main categories of services offered by banks.\",\"Incorrect. While banks do these activities, these are not the main categories of services offered by banks.\",\"Incorrect. While banks do theseactivities, these are not the main categories of services offered by banks.\"]','Deposit-taking, lending, money-transmission.','2020-01-08 10:18:09','2020-03-23 18:22:05'),
	(3,1,'Question 3–Why are Banks keen to Grow their Deposits?',2,'[\"So they can show higher liability balances on their balance sheet.\",\"To be able tolend more and increase its interest income and profitability.\",\"To pay their clients higher interest rates.\",\"Because that\\u2019s what banks do.\"]','[\"Incorrect. This is not the onlyreason for growing deposits. Banks grow deposits in order to lend more and increase theirinterest income, which usually leads to increased profitability.\",\"Correct. In addition to shareholders capital, banks rely on deposits for funding to lend to borrowers. The higher the deposits in a bank, the more it can lend, and the higher the interest income it earns.\",\"Incorrect. Interest paid on deposits is an expense to banks. They ultimately want to lend more to increase earnings from interest on loans, leading to increased profitability.\",\"Incorrect. There\\u2019s a reason for this. The higher thedeposits held by the bank, the more they can lend, and hence the higher the interestincomethey can earn, leading to greater profitability.\"]','To be able tolend more and increase its interest income and profitability.','2020-01-08 10:18:09','2020-03-23 18:47:32'),
	(4,1,'Question 4–In what ways do banks attract deposits?',2,'[\"By providing superior customer service for customer retention and growth.\",\"Providing security and efficiency.\",\"Advertising and positioning themselves as socially active in the community.\",\"All of the above.\"]','[\"Partially correct. Providing superior customer serviceis just one of the ways banks attract deposits.\",\"Partially correct. Providing security and efficiency is just one way banks attract deposits.\",\"Partially correct. Advertising and positioning themselves as socially active in the community is just one of the ways banks attract deposits.\",\"Correct. Banks work hard to raise deposits so that they can have more to lend to borrowers.\"]','All of the above.','2020-01-08 10:18:09','2020-03-23 18:49:24'),
	(5,1,'Question 5 –How do banks contribute to Kenya’s economic growth?',2,'[\"By providing customer service.\",\"Through money transmission and lending,which facilitates payments for goods and services thereby supporting production, trade, employment and other economic exchanges.\",\"By paying out cash when customers make withdrawal requests.\",\"By marketing which attracts more customers to the bank.\"]','[\"Incorrect. Providing customer service is part of what banks do, but does not specifically contribute to economic growth.\",\"Correct. Currency distribution and money circulation contribute to GDP and hence economic growth.\",\"Incorrect. Paying out customer withdrawals is not what contributes to economic growth per se. It\\u2019s what customers do withthe cash they withdraw and receive from other sources that contributes to economic growth.\",\"Incorrect. Marketing is part of what banks do, but does not directly contribute to economic growth.\"]','Through money transmission and lending,which facilitates payments for goods and services thereby supporting production, trade, employment and other economic exchanges.','2020-01-08 10:18:09','2020-03-23 18:51:07'),
	(6,2,'Non quia dolorem excepturi placeat illo et modi tempora dignissimos temporibus et.?',2,'[\"Reiciendis quisquam saepe.\",\"Iure omnis ut.\",\"Dolor.\",\"Laudantium dolor atque ut ea.\"]','[\"Iste vero assumenda vero nisi expedita dignissimos blanditiis quo.\",\"Ipsum officiis et assumenda modi voluptatibus.\",\"Labore corporis rerum fuga libero id provident officia.\",\"Perferendis atque et maiores sequi autem.\",\"Itaque qui quis voluptatem aut ut sed excepturi.\"]','Dolor.','2020-01-08 10:18:10','2020-01-08 10:18:10'),
	(7,2,'Laboriosam id vitae mollitia ut explicabo aut dolorum.?',2,'[\"Provident id.\",\"Voluptates quibusdam labore eligendi et adipisci labore.\",\"Enim voluptas debitis.\",\"Rerum exercitationem aliquam laboriosam autem earum ea.\"]','[\"Qui sed sed consequatur sit temporibus.\",\"Pariatur et enim omnis qui in quisquam.\",\"Qui dolor omnis doloribus.\",\"Accusantium qui excepturi ea et alias consectetur.\",\"Doloremque ut in voluptas qui.\",\"Officia sit quam a pariatur eligendi qui animi.\",\"Natus placeat nihil sed libero nostrum est quia.\"]','Voluptates quibusdam labore eligendi et adipisci labore.','2020-01-08 10:18:10','2020-01-08 10:18:10'),
	(8,2,'Qui quia ut iusto magnam aut suscipit nobis corrupti ea.?',2,'[\"Animi.\",\"Ut omnis corrupti porro molestias.\",\"Minima dolorum enim culpa et.\",\"Laudantium et expedita in aspernatur aut.\"]','[\"Modi amet adipisci minima quidem eaque.\",\"Quod culpa velit nihil quibusdam sed perferendis.\",\"Aspernatur quas autem eaque consequatur non nesciunt dolore impedit.\",\"Ad architecto tenetur sequi facere.\",\"Totam explicabo nemo sapiente sint.\",\"Soluta maxime laboriosam ipsa tempora unde dolorem eius consequuntur.\"]','Laudantium et expedita in aspernatur aut.','2020-01-08 10:18:10','2020-01-08 10:18:10'),
	(9,2,'Totam non maxime possimus nobis aut et.?',2,'[\"Dolorum.\",\"Officia architecto qui exercitationem eos sunt.\",\"Impedit perspiciatis deserunt.\",\"Esse ea.\"]','[\"Libero sed quia eaque non debitis.\",\"Quibusdam aperiam libero doloribus quia fugit.\",\"Enim voluptate quia aut praesentium facilis itaque.\",\"Id molestiae praesentium blanditiis enim.\",\"Minima sint qui voluptatibus a sit voluptates soluta.\",\"Qui sunt dicta doloribus iusto cum sapiente consectetur.\"]','Dolorum.','2020-01-08 10:18:10','2020-01-08 10:18:10'),
	(10,2,'Illum corrupti quod eaque quos.?',2,'[\"Corrupti.\",\"Dolore quaerat et quam necessitatibus.\",\"Ipsa recusandae vel corrupti iure.\",\"Illo.\"]','[\"Nisi consequatur iste id occaecati dolore.\",\"Asperiores necessitatibus iure aliquam perferendis dolore atque.\",\"Natus facilis porro et aperiam.\",\"Quibusdam soluta omnis magnam tenetur quidem.\",\"Ipsam nulla quos cumque enim modi eum quibusdam.\",\"Sed quasi modi quos eaque distinctio.\",\"Et qui ipsa quo iste et beatae.\",\"Ipsa reiciendis nemo a nam molestias laboriosam.\"]','Dolore quaerat et quam necessitatibus.','2020-01-08 10:18:10','2020-01-08 10:18:10'),
	(11,2,'Voluptas veniam qui est dolor consequatur nihil architecto.?',2,'[\"Sint officia qui ullam.\",\"Quod nostrum.\",\"Quia sit dolor.\",\"Rerum fugit ullam facilis quia.\"]','[\"Quia porro debitis quibusdam et.\",\"Quae similique eum eum modi laboriosam.\",\"In est odio corrupti molestias optio.\",\"In rem est optio quo voluptatem placeat.\",\"Esse molestiae corporis aut ut quis voluptatem earum recusandae.\",\"Maiores nisi doloribus esse eveniet aspernatur.\"]','Rerum fugit ullam facilis quia.','2020-01-08 10:18:10','2020-01-08 10:18:10'),
	(12,2,'Facilis debitis est enim est maiores ea nisi quisquam provident dolor sunt.?',2,'[\"Quidem a cumque accusamus nesciunt.\",\"Nam quia et.\",\"Velit sapiente culpa.\",\"Qui qui omnis eum.\"]','[\"Iste debitis a consequatur eveniet excepturi dolorem id.\",\"Culpa voluptatibus placeat quo unde ea commodi consequatur.\",\"Officiis minus deleniti rerum quod iste.\",\"Occaecati in ut perspiciatis aut.\",\"Adipisci vel ipsum et in excepturi nobis.\",\"In ut aut architecto suscipit vitae suscipit.\",\"Quis ipsam et voluptate delectus consequatur qui.\",\"Id aut sed id possimus aut rerum dolores corporis.\",\"Sed non quia sed repellat qui voluptas maiores.\",\"Unde assumenda in rerum nemo ut sed placeat.\"]','Qui qui omnis eum.','2020-01-08 10:18:10','2020-01-08 10:18:10'),
	(13,2,'Id sint consectetur possimus vitae tempora mollitia vel sit quia adipisci soluta dignissimos.?',2,'[\"Fuga temporibus.\",\"Cumque voluptas laudantium.\",\"Praesentium beatae ad quos.\",\"Nam inventore eum.\"]','[\"Sed debitis eaque consectetur et repudiandae ut.\",\"Sint dolores sed exercitationem.\",\"Ipsam culpa tenetur ut voluptate totam earum.\",\"Voluptas vitae natus aliquid ut id aspernatur veniam maxime.\",\"Quia quam velit officiis repellat id et unde.\",\"Et ratione est esse architecto ab.\",\"Est et nam facere ut quia sint qui.\"]','Cumque voluptas laudantium.','2020-01-08 10:18:10','2020-01-08 10:18:10'),
	(14,3,'Voluptates iste ipsa ut magnam velit porro dolores tenetur occaecati aliquam dolores doloribus nisi.?',2,'[\"Sapiente.\",\"Maiores atque sapiente architecto illo.\",\"Eligendi quae et dolore.\",\"Fuga impedit minima dolores.\"]','[\"Quibusdam exercitationem non enim.\",\"Amet voluptas impedit totam accusantium vero possimus dolorem molestias.\",\"Culpa ut qui quidem voluptatem animi et voluptatem.\",\"Quod voluptatem minus molestiae eligendi expedita occaecati deleniti.\"]','Sapiente.','2020-01-08 10:18:10','2020-01-08 10:18:10'),
	(15,3,'Eaque quibusdam enim ducimus amet quibusdam.?',2,'[\"Omnis quis.\",\"Voluptas temporibus aut voluptate illum.\",\"Nihil ad dolores sit deleniti animi.\",\"Est.\"]','[\"Molestiae molestias maiores quae.\",\"Consequatur sunt quia cum delectus.\",\"Laudantium ut laudantium maxime veniam quisquam vitae.\",\"Et et nulla nemo voluptatem repudiandae consequatur doloremque perferendis.\",\"Assumenda eos sed cum.\",\"Repudiandae doloribus maiores ipsa autem iure.\",\"Cumque ut quis ut autem omnis consequatur magni.\"]','Est.','2020-01-08 10:18:10','2020-01-08 10:18:10'),
	(16,3,'Asperiores dignissimos consequuntur similique tempore.?',2,'[\"In.\",\"Repellat.\",\"Voluptas ipsa eaque sit laborum.\",\"Temporibus voluptates.\"]','[\"Officiis adipisci voluptatibus sed veniam quia explicabo et qui.\",\"Aut voluptates cum aut eaque hic delectus omnis.\",\"Explicabo doloribus ut minima quia quibusdam error.\",\"Maiores odit ab accusamus ut necessitatibus excepturi.\",\"Error asperiores est dolores sunt vel ut est.\",\"Qui quasi sit sed incidunt fuga.\",\"Error facilis ut ducimus.\",\"Itaque alias qui molestiae molestiae possimus ut consequatur aliquid.\"]','Repellat.','2020-01-08 10:18:10','2020-01-08 10:18:10'),
	(17,3,'Placeat accusantium nam aut voluptate odio ex est sequi vel quisquam deleniti.?',2,'[\"Eos earum.\",\"Ratione iste sit accusantium dolorem.\",\"Ut illo quo aliquid soluta.\",\"Est nemo nisi ut quibusdam.\"]','[\"Nemo quia nesciunt odit ut iusto occaecati at.\",\"Laboriosam vel ut blanditiis corporis voluptatem.\",\"Eos eaque nisi vel ad et corporis ipsa.\",\"Magnam sed ut dolor occaecati est.\",\"Corporis est ea qui eum voluptas in.\",\"Et quo autem ratione excepturi expedita.\",\"Enim dolorum ea id quisquam quod sit cupiditate consectetur.\",\"Aut laborum voluptatum quae ut.\"]','Est nemo nisi ut quibusdam.','2020-01-08 10:18:10','2020-01-08 10:18:10'),
	(18,3,'Qui porro unde repellendus quaerat.?',2,'[\"Dolor.\",\"Qui molestiae.\",\"Est natus nulla.\",\"Cumque asperiores eligendi.\"]','[\"Quo aut velit in odio ipsam sit doloribus.\",\"Necessitatibus aut officiis vero et atque dolorem.\",\"Quo voluptatem non architecto consequuntur asperiores.\",\"Minima velit explicabo hic consequatur minima eos dolores.\",\"Quis cumque quo id distinctio doloribus sapiente.\",\"Sapiente quos consequatur iste quam omnis et.\"]','Qui molestiae.','2020-01-08 10:18:10','2020-01-08 10:18:10'),
	(19,3,'Ea natus perspiciatis corporis ut nam voluptas.?',2,'[\"Sunt corporis eum ea minima ipsam est.\",\"Labore quam error facilis est.\",\"Id doloremque.\",\"Eos amet.\"]','[\"Voluptatem quae ut ea eius velit.\",\"Et nisi provident dolor sit omnis ut.\",\"Culpa nesciunt autem neque quo mollitia doloribus.\",\"Doloribus architecto quisquam sit.\"]','Sunt corporis eum ea minima ipsam est.','2020-01-08 10:18:10','2020-01-08 10:18:10'),
	(20,4,'Who is the main facilitator of flow of money in Kenya?',2,'[\"The public\",\"Businesses\",\"Banks\",\"The government\"]','[\"Incorrect.The public are part of the flowof money in Kenya, but they do not facilitate it\",\"Incorrect.Businesses are part of the flowof money in Kenya, but they do not facilitate it.\",\"Correct! They facilitate flow of money in Kenyathrough collecting depositors\\u2019money and paying it out through loans and withdrawals.\",\"Incorrect. The government is part of the flow and has an oversight role in the financial system, but they do not facilitate it.\"]','Banks','2020-03-23 16:58:43','2020-03-23 16:58:43'),
	(21,4,'Which of the following lists contain the main players of the banking system in Kenya?',2,'[\"Capital Markets Authority, Insurance companies, National Treasury\",\"Kenya Institute of Bankers, Association of Micro-Finance Institutions (AMFI), Central Bank of Kenya\",\"Banks, National Treasury, Central Bank of Kenya, Kenya Deposit Insurance Corporation.\",\"Saccos, Nairobi Securities Exchange, Banks.\"]','[\"Incorrect. While important, theCapital Markets Authority is not the main player in the banking system in Kenya.\",\"Incorrect.While important, the Kenya Institute of Bankers, Association of Micro-Finance Institutions (AMFI) and the Central Bank of Kenya are not the main players of the banking system in Kenya.\",\"This answer is correct. These are the main players in the Kenyan banking system.\",\"Incorrect. While important, Saccos, Nairobi Securitis Exchange and Banks are not the main players in the Kenyan banking system.\"]','Banks, National Treasury, Central Bank of Kenya, Kenya Deposit Insurance Corporation.','2020-03-23 17:02:10','2020-03-23 17:02:10'),
	(22,4,'In Kenya’s banking system, what role is played by the National Treasury?',2,'[\"Government spending and the financing of it through taxes and donor funds (loans and grants).\",\"National development planning and economic policy.\",\"Supervising bank activities.\",\"Money transmission within the economy.\"]','[\"Correct.\",\"Incorrect. National development planning is carried out by the Ministry of Devolution and Planning.\",\"Incorrect. Supervising of banks is carried out by the Central Bank of Kenya.\",\"Incorrect. Money transmission is carried out by banks.\"]','Government spending and the financing of it through taxes and donor funds (loans and grants).','2020-03-23 17:04:39','2020-03-23 17:04:39'),
	(23,4,'What roledoes the Central Bank of Kenya play?',2,'[\"Supervisor and regulator of banks.\",\"Banker to the government.\",\"Lender of last resort to banks.\",\"All of the above.\"]','[\"Partially correct. The Central Bank of Kenya plays other roles.\",\"Partially correct. The Central Bank of Kenya plays other roles.\",\"Partially correct. The Central Bank of Kenya plays other roles.\",\"Correct. The Central bank of Kenya plays all the above roles: Supervisor and regulator of banks, banker to the government, and lender of last resort to banks.\"]','All of the above.','2020-03-23 17:07:22','2020-03-23 17:07:22'),
	(24,4,'What role is NOT played by the Kenya Deposit Insurance Corporation?',2,'[\"Protector of depositors\\u2019 funds in banks.\",\"Lender to banks.\",\"Monitoring of banks.\",\"Resolution of banks under stress.\"]','[\"Incorrect. Protecting depositors\\u2019 funds through deposit insurance is part of the Kenya Deposit Insurance Corporation\\u2019s role.\",\"Correct. KDIC does NOT play this role. The Central Bank of Kenya is the lender of last resort to banks.\",\"Incorrect. Surveillance of banks is one of the roles played by the Kenya Deposit Insurance Corporation.\",\"Incorrect. Resolutions of banks under stress is one of the roles of the Kenya Deposit Insurance Corporation.\"]','Lender to banks.','2020-03-23 17:09:41','2020-03-23 17:09:41'),
	(25,1,'Question 6 –What is fiduciary responsibility?',2,'[\"Providing insurance to customers.\",\"Provision of sound financial advice to clients.\",\"The legal, ethical responsibility of banks to act in the best interests of its customers.\",\"Responsibility to the government in carrying out business.\"]','[\"Incorrect. While many banks provide insurance to customers, this is not what fiduciary responsibility is.\",\"Partially correct. This is part of fiduciary responsibility, but does not describe it entirely.\",\"Correct. Banks need to act in the best interests of its customers, which includesproviding sound advice, exercising due diligence and taking care of customers\\u2019 money.\",\"Incorrect. Responsibility to the government in carrying out business is part of a banks responsibility, however the focus of the banks fiduciary responsibility is to the customer, with the government, overseeing this responsibility through the Central Bankof Kenya.\"]','The legal, ethical responsibility of banks to act in the best interests of its customers.','2020-03-23 18:52:48','2020-03-23 18:52:48'),
	(26,1,'Question 7 –Which of the following is NOT a pillar of good governance?',2,'[\"Accountability\",\"Transparency\",\"Shareholder value\",\"Responsibility\",\"Efficiency\",\"Integrity\"]','[\"Incorrect. Accountability is a pillar of good governance.\",\"Incorrect. Transparency is a pillar of good governance.\",\"Correct. Shareholder value is not a pillar of good governance according to the Governance Institute. It is covered under the other pillars, including Accountability.\",\"Incorrect. Responsibility is a pillar of good governance.\",\"Incorrect. Efficiency is a pillar of good governance.\",\"Incorrect. Integrity is a pillar of good governance.\"]','Shareholder value','2020-03-23 18:55:51','2020-03-23 18:55:51');

/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table role_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role_user`;

CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;

INSERT INTO `role_user` (`id`, `role_id`, `user_id`)
VALUES
	(1,3,1),
	(2,1,2),
	(3,2,3),
	(4,2,3),
	(5,1,4),
	(6,1,5),
	(7,1,6),
	(8,1,7),
	(9,1,8),
	(10,1,9),
	(11,1,10),
	(12,1,11),
	(13,1,12),
	(14,1,13);

/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`)
VALUES
	(1,'student','Student to learn course','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(2,'instructor','Instructor to manage course','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(3,'admin','Admin to manage the site','2020-01-08 10:18:09','2020-01-08 10:18:09');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table scores
# ------------------------------------------------------------

DROP TABLE IF EXISTS `scores`;

CREATE TABLE `scores` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `score` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `scores` WRITE;
/*!40000 ALTER TABLE `scores` DISABLE KEYS */;

INSERT INTO `scores` (`id`, `user_id`, `question_id`, `answer`, `score`, `created_at`, `updated_at`)
VALUES
	(1,4,1,'Deposit-taking, lending, money-transmission.',0,'2020-01-09 12:11:22','2020-01-09 12:11:22'),
	(2,4,2,'To pay their clients higher interest rates.',0,'2020-01-09 12:25:04','2020-01-09 12:25:04'),
	(3,4,3,'Through lending which facilitates payments for goods and services thereby supporting production, trade, employment and other economic exchanges.',2,'2020-01-09 12:26:09','2020-01-09 12:26:09'),
	(4,4,4,'The legal, ethical relationship of trust leading banks to act in the best interests of its customers.',2,'2020-01-09 12:26:43','2020-01-09 12:26:43'),
	(5,4,5,'Providing security/ assurance of safety.',0,'2020-01-09 12:26:54','2020-01-09 12:26:54'),
	(6,2,1,'Deposit-taking, lending, money-transmission.',2,'2020-01-18 02:33:16','2020-01-18 02:33:16'),
	(7,2,2,'To pay their clients higher interest rates.',0,'2020-01-18 02:33:19','2020-01-18 02:33:19'),
	(8,2,3,'Through lending which facilitates payments for goods and services thereby supporting production, trade, employment and other economic exchanges.',2,'2020-01-18 02:33:24','2020-01-18 02:33:24'),
	(9,11,1,'Deposit-taking, lending, money-transmission.',2,'2020-03-20 10:15:16','2020-03-20 10:15:16'),
	(10,11,2,'Because that’s what banks do.',0,'2020-03-20 10:15:20','2020-03-20 10:15:20'),
	(11,11,3,'Through lending which facilitates payments for goods and services thereby supporting production, trade, employment and other economic exchanges.',2,'2020-03-20 10:15:31','2020-03-20 10:15:31'),
	(12,11,4,'Responsibility to the government in carrying out business.',0,'2020-03-20 10:15:47','2020-03-20 10:15:47'),
	(13,11,5,'Improved customer service – convenience, and efficiency.',0,'2020-03-20 10:15:50','2020-03-20 10:15:50'),
	(14,13,20,'Banks',2,'2020-05-12 10:56:45','2020-05-12 10:56:45'),
	(15,13,21,'Kenya Institute of Bankers, Association of Micro-Finance Institutions (AMFI), Central Bank of Kenya',0,'2020-05-12 10:59:33','2020-05-12 10:59:33'),
	(16,13,22,'Government spending and the financing of it through taxes and donor funds (loans and grants).',2,'2020-05-12 11:01:00','2020-05-12 11:01:00'),
	(17,13,23,'All of the above.',2,'2020-05-12 11:01:14','2020-05-12 11:01:14'),
	(18,13,24,'Lender to banks.',2,'2020-05-12 11:01:34','2020-05-12 11:01:34');

/*!40000 ALTER TABLE `scores` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table transactions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `transactions`;

CREATE TABLE `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_method` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;

INSERT INTO `transactions` (`id`, `user_id`, `course_id`, `amount`, `status`, `payment_method`, `order_details`, `created_at`, `updated_at`)
VALUES
	(1,1,1,0.00,'completed','paypal_express_checkout','{\"TOKEN\":\"success\",\"status\":\"succeeded\",\"Timestamp\":1561787415,\"ACK\":\"Success\"}','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(2,2,1,0.00,'completed','paypal_express_checkout','{\"TOKEN\":\"success\",\"status\":\"succeeded\",\"Timestamp\":1578478840,\"ACK\":\"Success\"}','2020-01-08 10:20:40','2020-01-08 10:20:40'),
	(3,3,1,0.00,'completed','paypal_express_checkout','{\"TOKEN\":\"success\",\"status\":\"succeeded\",\"Timestamp\":1578484534,\"ACK\":\"Success\"}','2020-01-08 11:55:34','2020-01-08 11:55:34'),
	(4,4,1,0.00,'completed','paypal_express_checkout','{\"TOKEN\":\"success\",\"status\":\"succeeded\",\"Timestamp\":1578506312,\"ACK\":\"Success\"}','2020-01-08 17:58:32','2020-01-08 17:58:32'),
	(5,5,1,0.00,'completed','paypal_express_checkout','{\"TOKEN\":\"success\",\"status\":\"succeeded\",\"Timestamp\":1579244541,\"ACK\":\"Success\"}','2020-01-17 07:02:21','2020-01-17 07:02:21'),
	(6,6,1,0.00,'completed','paypal_express_checkout','{\"TOKEN\":\"success\",\"status\":\"succeeded\",\"Timestamp\":1579244549,\"ACK\":\"Success\"}','2020-01-17 07:02:28','2020-01-17 07:02:29'),
	(7,7,1,0.00,'completed','paypal_express_checkout','{\"TOKEN\":\"success\",\"status\":\"succeeded\",\"Timestamp\":1579266922,\"ACK\":\"Success\"}','2020-01-17 13:15:22','2020-01-17 13:15:22'),
	(8,8,1,0.00,'completed','paypal_express_checkout','{\"TOKEN\":\"success\",\"status\":\"succeeded\",\"Timestamp\":1579494173,\"ACK\":\"Success\"}','2020-01-20 04:22:53','2020-01-20 04:22:53'),
	(9,9,1,0.00,'completed','paypal_express_checkout','{\"TOKEN\":\"success\",\"status\":\"succeeded\",\"Timestamp\":1582557029,\"ACK\":\"Success\"}','2020-02-24 15:10:29','2020-02-24 15:10:29'),
	(10,10,1,0.00,'completed','paypal_express_checkout','{\"TOKEN\":\"success\",\"status\":\"succeeded\",\"Timestamp\":1584698663,\"ACK\":\"Success\"}','2020-03-20 10:04:23','2020-03-20 10:04:23'),
	(11,11,1,0.00,'completed','paypal_express_checkout','{\"TOKEN\":\"success\",\"status\":\"succeeded\",\"Timestamp\":1584699096,\"ACK\":\"Success\"}','2020-03-20 10:11:36','2020-03-20 10:11:36'),
	(12,12,1,0.00,'completed','paypal_express_checkout','{\"TOKEN\":\"success\",\"status\":\"succeeded\",\"Timestamp\":1585053300,\"ACK\":\"Success\"}','2020-03-24 12:35:00','2020-03-24 12:35:00'),
	(13,13,1,0.00,'completed','paypal_express_checkout','{\"TOKEN\":\"success\",\"status\":\"succeeded\",\"Timestamp\":1589280165,\"ACK\":\"Success\"}','2020-05-12 10:42:44','2020-05-12 10:42:45');

/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `is_active`, `remember_token`, `bank`, `created_at`, `updated_at`)
VALUES
	(1,'Admin','Account','admin@gmail.com','$2y$10$QIWdy/hVLXF51MmZ/4vNi.Ys.Kyo1d87DmCFh3j2i87K2gXLD8FnK',1,NULL,'','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(2,'Student','Account','student@gmail.com','$2y$10$I86NUs/7TfPsTE0gH9s8FemUsJIksrfOAodw1d4H1ibgLmLjdn7gS',1,NULL,'','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(3,'Instructor','Account','instructor@gmail.com','$2y$10$TZYfMnaJfdJpxH5oOHCZ8.GEFIazTESSHlTHZbuUvtN6EPf0zxRxm',1,NULL,'','2020-01-08 10:18:09','2020-01-08 10:18:09'),
	(4,'Franc','Kabu','franc.kabu@gmail.com','$2y$10$NCuJmNSSKDgUdbzuLGBkbubcCUDxctgEJayIVDr3Fdrej0D19P3ya',1,NULL,'KCB','2020-01-08 17:57:56','2020-01-08 17:57:56'),
	(5,'Edgar','Herman','edgar@tdm.co.ke','$2y$10$qwevZg262a73j634aDq5jO05/AHzmc0eDelTHBIsMQKrVJb58kCcG',1,NULL,'CBA','2020-01-17 07:01:31','2020-01-17 07:01:31'),
	(6,'Carol','Mwazi','cmwazi@gmail.com','$2y$10$QtHdsvdUe0Ax.ht1FuB2p..56tVmKT4UG/avN8R2riczokEpHZlJS',1,'dOKtLz07DCPpEMorZji3iKxFo8LZgtj3rj6x9o3U5M5S7EeVt1s1zfibcypa','KCB','2020-01-17 07:01:34','2020-03-20 09:24:19'),
	(7,'Shantale','Mureithi','shantale@bsd.co.ke','$2y$10$BZIy1WrTxwtLmeaBP/ZOTOut1ruFE/l9ULoDDr2d/SmK2j50EUpZ2',1,'OgdxrnOCNyRvJpt4UYUITpS4wzlDoFn73bLaGxtUMCqaAozK7uisXRn8qmya','CBA','2020-01-17 13:14:49','2020-04-28 11:35:30'),
	(8,'Shantale','Mutune','shantale@gmail.com','$2y$10$PSZjRDbVkpZrU3jfB0Z.BeEhjjQ9PqBSLx87JhlKVLm/7qJr/yIqa',1,NULL,'CBA','2020-01-20 04:22:48','2020-01-20 04:22:48'),
	(9,'Anna','Nyareru','wagachoki@yahoo.com','$2y$10$TMmvkeg79GjM/bY5WB39AuCBlI/ioeLqNbYtFbV.sAcEm1uBeLvza',1,NULL,'SC','2020-02-24 15:07:33','2020-02-24 15:07:33'),
	(10,'Nelson','Ameyo','nelson@tdm.co.ke','$2y$10$ouEH98OxzvfTe8z4hKXEJ./.tcEFiGnoaYSktQSwuThHnWIr0zula',1,NULL,'CBA','2020-03-20 09:58:19','2020-03-20 09:58:19'),
	(11,'Kevin','Njoroge','kev@tdm.co.ke','$2y$10$ZrQVaEaJi.8ibp5K1AGRJegz1AHW8fl3pRQGpy903rHXY3k0aSYUu',1,NULL,'CBA','2020-03-20 10:10:06','2020-03-20 10:10:06'),
	(12,'Nelson','Nyongesa','nnyongesa@kdic.go.ke','$2y$10$uGHeVSv8LclxL9E8BLoloOCwErkNsJrbIkfJiUmXo7VTdszQv.vFm',1,NULL,'KCB','2020-03-24 12:34:28','2020-03-24 12:34:28'),
	(13,'Carol','Mwanzi','carol@mwanzi.co.ke','$2y$10$W1EuA4rmgjHDs4TMRRVShuZwf5IRh4N..8Zlz5NqYQKRUvU7gQULW',1,NULL,'CBA','2020-05-12 10:30:15','2020-05-12 10:30:15');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table withdraw_requests
# ------------------------------------------------------------

DROP TABLE IF EXISTS `withdraw_requests`;

CREATE TABLE `withdraw_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `instructor_id` int(11) NOT NULL,
  `paypal_id` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0-pending,1-processed',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
