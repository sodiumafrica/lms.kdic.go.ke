# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: kdic.tdm.co.ke (MySQL 5.5.5-10.1.43-MariaDB-0ubuntu0.18.04.1)
# Database: lms.kdic.go.ke
# Generation Time: 2019-12-02 02:13:00 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table courses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `courses`;

CREATE TABLE `courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `instructor_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `instruction_level_id` int(10) unsigned NOT NULL,
  `course_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci,
  `overview` text COLLATE utf8mb4_unicode_ci,
  `course_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course_video` int(10) unsigned DEFAULT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  `strike_out_price` decimal(8,2) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;

INSERT INTO `courses` (`id`, `instructor_id`, `category_id`, `instruction_level_id`, `course_title`, `course_slug`, `keywords`, `overview`, `course_image`, `thumb_image`, `course_video`, `duration`, `price`, `strike_out_price`, `is_active`, `created_at`, `updated_at`)
VALUES
	(1,1,1,1,'Deposit Insurance Training for Bankers','deposit-insurance-training-for-bankers','Banks ,Training,Insuarance','<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using , making it look like readable English.</p>\r\n<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don`t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn`t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.</p>','placeholder.png','placeholder.png',NULL,'2 days',NULL,NULL,1,'2019-11-26 09:04:14','2019-11-26 09:12:04');

/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table curriculum_lectures_quiz
# ------------------------------------------------------------

DROP TABLE IF EXISTS `curriculum_lectures_quiz`;

CREATE TABLE `curriculum_lectures_quiz` (
  `lecture_quiz_id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `contenttext` text COLLATE utf8mb4_unicode_ci,
  `media` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_type` int(11) DEFAULT NULL COMMENT '0-video,1-audio,2-document,3-text',
  `sort_order` int(11) DEFAULT NULL,
  `publish` int(11) NOT NULL DEFAULT '0',
  `resources` text COLLATE utf8mb4_unicode_ci,
  `createdOn` datetime NOT NULL,
  `updatedOn` datetime NOT NULL,
  PRIMARY KEY (`lecture_quiz_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `curriculum_lectures_quiz` WRITE;
/*!40000 ALTER TABLE `curriculum_lectures_quiz` DISABLE KEYS */;

INSERT INTO `curriculum_lectures_quiz` (`lecture_quiz_id`, `section_id`, `type`, `title`, `description`, `contenttext`, `media`, `media_type`, `sort_order`, `publish`, `resources`, `createdOn`, `updatedOn`)
VALUES
	(1,1,NULL,'Ducimus illo optio est debitis ipsa.','Quisquam et commodi facilis voluptas sint assumenda repudiandae ab ab asperiores.','','1',0,1,0,'[3]','2019-11-26 09:04:14','2019-11-26 09:04:14'),
	(2,1,NULL,'Corrupti harum itaque dicta voluptatem.','Magnam quas veritatis nostrum explicabo quia velit dicta illum sapiente fuga quia voluptatum harum tempora amet quis suscipit et dolor commodi in distinctio deserunt omnis enim tenetur officia perferendis delectus neque et in quia animi sint reprehenderit itaque magnam sit commodi atque modi hic minus unde nihil deserunt voluptatum ab occaecati ducimus dolor facere quis ab omnis voluptates id quis cupiditate maiores incidunt rerum autem deleniti deleniti.','','1',1,2,1,'[3]','2019-11-26 09:04:14','2019-11-26 09:04:14'),
	(3,1,NULL,'Laboriosam in voluptas quam pariatur architecto sapiente.','Et in dolores aut ducimus enim rerum et suscipit sunt repudiandae veniam at ut dolorem voluptas dicta asperiores suscipit a ut adipisci natus provident est.','','2',2,3,1,'[4]','2019-11-26 09:04:15','2019-11-26 09:04:15'),
	(4,1,NULL,'Quidem omnis iste molestiae earum rerum tempore.','Voluptas laborum repellendus facere qui minima tempora deleniti architecto excepturi perferendis praesentium quia dolores voluptate rerum dolor quibusdam dicta veniam eum aut blanditiis aut a fugiat dolores illo porro itaque aut dolor sunt nihil non sed consectetur voluptates qui et optio inventore dicta ipsa sit necessitatibus nesciunt.','Est labore eum veritatis fuga eius illum modi iusto molestias quia doloremque eum accusantium ut culpa.',NULL,3,4,1,'[5]','2019-11-26 09:04:15','2019-11-26 09:04:15'),
	(12,3,NULL,'Sapiente dolorem asperiores.','Reprehenderit laudantium velit alias aut magnam quidem assumenda iusto consequatur ipsam consequatur laboriosam eum non sapiente fugit error distinctio velit et quia corrupti sed aut ducimus atque ducimus dolore.','Nulla hic est qui natus officiis animi ullam sed velit atque amet illum accusamus maxime odio qui temporibus quo omnis qui similique repellendus nesciunt eaque sed consequuntur aut.',NULL,3,7,1,'[5]','2019-11-26 09:04:15','2019-11-26 09:04:15'),
	(19,2,0,'Review Questions',NULL,'<ol>\n<li>What is financial intermediation?</li>\n<li>From the following list, what are roles of banks? Tick all that apply.</li>\n<li>Why are banks keen to grow their deposits?</li>\n<li>How does lending create money?</li>\n<li>What is fiduciary responsibility?</li>\n<li>Which of the following is NOT a pillar of good corporate governance?</li>\n<li>Why are clients reluctant to deposit their money with banks?</li>\n<li>What have banks done to increase deposits?</li>\n</ol>',NULL,3,7,1,NULL,'2019-11-26 14:53:58','2019-11-26 15:21:08'),
	(20,2,0,'Module Objectives',NULL,'<p>By the end of this module you will be able to:-</p>\n<ul>\n<li>Define financial intermediation.</li>\n<li>Explain 3 ways banks provide financial intermediation.</li>\n<li>Outline other roles banks play.</li>\n<li>Explain the responsibilities of banks.</li>\n<li>Actions banks have taken to increase deposits.</li>\n</ul>\n<p> </p>',NULL,3,1,1,NULL,'2019-11-26 15:21:34','2019-11-26 15:21:34'),
	(21,3,0,'Module Outline',NULL,NULL,NULL,NULL,8,0,NULL,'2019-11-26 15:23:23','2019-11-26 15:23:23'),
	(22,2,0,'Module Outline',NULL,'<p>- Financial intermediation</p>\n<p>- Roles and responsibilities of banks</p>\n<p>- How banks increase deposits.</p>',NULL,3,2,1,NULL,'2019-11-26 15:24:02','2019-11-26 15:24:02'),
	(24,2,0,'Roles of Banks',NULL,'<p><span style=\"text-decoration: underline;\"><strong style=\"user-select: auto;\">Receiving and safe-keeping of client deposits</strong></span></p>\n<p style=\"user-select: auto;\">Various types, including current, savings or fixed deposits. These are assets to the depositors and liabilities to the banks.<br style=\"user-select: auto;\" />The higher the deposits, the more the bank can lend its clients.</p>\n<p style=\"user-select: auto;\"><span style=\"text-decoration: underline;\"><strong>Lending</strong></span></p>\n<p style=\"user-select: auto;\">Individual or organisational borrowers. Money from capital and deposits.<br style=\"user-select: auto;\" />Core income earner for banks.<br style=\"user-select: auto;\" />Important consideration: Risk Management.</p>\n<p style=\"user-select: auto;\"><span style=\"text-decoration: underline;\"><strong>Money transmission</strong></span></p>\n<p>Most Kenyans do not trust banks as a safe place to keep their money.<br />The ones with bank accounts do not deposit often/ everything and they are',NULL,3,4,1,NULL,'2019-11-26 15:26:14','2019-11-26 15:26:14'),
	(25,2,0,'Financial Intermediation',NULL,'<p>How banks provide financial intermediation:<br />- Receiving and safe-keeping of client deposits.<br />- Lending.<br />- Money transmission.</p>\n<p>The above 3 activities banks facilitate:-<br />- Money creation.<br />- Development and growth of the economy.<br />- Money circulation through currency distribution.</p>',NULL,3,3,1,NULL,'2019-11-26 15:28:49','2019-11-26 15:40:01'),
	(32,2,0,'Actions Banks have Taken to Raise Deposits',NULL,'<ul>\n<li>Increasing their visibility and positioning themselves as socially active and engaged with customers;</li>\n<li>Improving security and efficiency and;</li>\n<li>Going all-out to provide superior customer service.</li>\n</ul>',NULL,3,5,1,NULL,'2019-11-26 15:41:24','2019-11-26 15:41:24'),
	(33,2,0,'Financial  Intermediation Graphic',NULL,'<p>Financial intermediation graphic: Customer banks 100/=. Does not need it immediately. Borrower borrows and banks in bank and uses to make payments. If the chain is broken there is panic and a run on deposits. So banks must exercise prudent risk management. This is supervised by the Central Bank.</p>',NULL,3,6,1,NULL,'2019-11-26 15:42:19','2019-11-26 15:43:26');

/*!40000 ALTER TABLE `curriculum_lectures_quiz` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table curriculum_sections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `curriculum_sections`;

CREATE TABLE `curriculum_sections` (
  `section_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) DEFAULT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `createdOn` datetime NOT NULL,
  `updatedOn` datetime NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `curriculum_sections` WRITE;
/*!40000 ALTER TABLE `curriculum_sections` DISABLE KEYS */;

INSERT INTO `curriculum_sections` (`section_id`, `course_id`, `title`, `sort_order`, `createdOn`, `updatedOn`)
VALUES
	(2,1,'Module 2',1,'2019-11-26 09:04:15','2019-11-26 14:28:55');

/*!40000 ALTER TABLE `curriculum_sections` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
